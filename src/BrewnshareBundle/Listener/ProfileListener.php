<?php

namespace BrewnshareBundle\Listener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use BrewnshareBundle\Entity\Address;
use BrewnshareBundle\Entity\Equipment;

/**
 * Listen on FOS Events
 */
class ProfileListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::PROFILE_EDIT_INITIALIZE => 'onProfileEditInitialize'
        );
    }

    public function onProfileEditInitialize(GetResponseUserEvent $event)
    {
        $user = $event->getUser();
        
        if(!$user->getMainAddress()) {
            $address = new Address();
            $address->setUser($user);
//            $address->setCountry(strtoupper($event->getRequest()->getLocale())); // Setting default country from locale
            $user->setMainAddress($address);
        }
        
        // Setting autocomplete field value
        $autocomplete = "";
        if(!is_null($user->getMainAddress()->getNumber()))
            $autocomplete .= $user->getMainAddress()->getNumber().' ';
        if(!is_null($user->getMainAddress()->getAddress()))
            $autocomplete .= $user->getMainAddress()->getAddress().', ';
        if(!is_null($user->getMainAddress()->getCity()))
            $autocomplete .= $user->getMainAddress()->getCity().', ';
        if(!is_null($user->getMainAddress()->getCountry()))
            $autocomplete .= $user->getMainAddress()->getCountry();
        
        $user->getMainAddress()->setMapsAutocomplete($autocomplete);
        
        if(!$user->getEquipment()) {
            $equipment = new Equipment();
            $equipment->setUser($user);
            $user->setEquipment($equipment);
        }
    }
}