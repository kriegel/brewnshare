<?php

namespace BrewnshareBundle\Listener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\GetResponseUserEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Routing\Router;

/**
 * Listen on FOS Events
 */
class RegistrationListener implements EventSubscriberInterface
{
    protected $authorizationChecker;
    protected $router;
    
    public function __construct(AuthorizationChecker $authorizationChecker, Router $router) {
        $this->authorizationChecker = $authorizationChecker;
        $this->router = $router;
    }
    
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_INITIALIZE => 'onRegistrationInitialize'
        );
    }

    public function onRegistrationInitialize(GetResponseUserEvent $event)
    {
        if($this->authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $response = new RedirectResponse($this->router->generate('brewnshare_homepage'));
            $event->setResponse($response);
        }
    }
}