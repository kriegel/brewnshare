<?php

namespace BrewnshareBundle\Listener;
 
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use BrewnshareBundle\Model\UserControllerInterface;
 
class BeforeControllerListener
{
    protected $em;
    protected $twig;
    protected $authorizationChecker;
 
    public function __construct(EntityManager $em, \Twig_Environment $twig, AuthorizationChecker $authorizationChecker)
    {
        $this->em = $em;
        $this->twig = $twig;
        $this->authorizationChecker = $authorizationChecker;
    }
    
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();
        $request = $event->getRequest();

        if (!is_array($controller)) {
            // not an object but a different kind of callable. Do nothing
            return;
        }

        $controllerObject = $controller[0];

        // skip initializing for exceptions
        if ($controllerObject instanceof ExceptionController) {
            return;
        }
        
        /**
         * If instanceof UserControllerInterface, setting BaseUserController $routeUser variable and setting Twig's variable routeUser
         */
        if ($controllerObject instanceof UserControllerInterface && $event->isMasterRequest()) {
            if($this->authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
                $userSlug = $request->attributes->get('slug_user');

                if(!is_null($userSlug)) {
                    $user = $this->em->getRepository('BrewnshareBundle:User')->findOneBy(array('slug' => $userSlug, 'enabled' => true));

                    if(!$user)
                        throw new NotFoundHttpException("Unable to find User");

                    $controllerObject->setRouteUser($user);
                }
                else
                    $controllerObject->setDefaultUser();

                // Passing user to global twig variable
                $this->twig->addGlobal('routeUser', $controllerObject->getRouteUser());
                $this->twig->addGlobal('isCurrentUser', $controllerObject->isCurrentUser());
            }
        }
    }
}