<?php

namespace BrewnshareBundle\Listener;

use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
 
class LoginRedirectionListener
{
    protected $dispatcher;
    protected $authorizationChecker;
    protected $router;
 
    public function __construct($dispatcher, AuthorizationChecker $authorizationChecker, Router $router)
    {
        $this->dispatcher = $dispatcher;
        $this->authorizationChecker = $authorizationChecker;
        $this->router = $router;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        // On demande a écouter une fois l'évènement kernel.response
        $this->dispatcher->addListener(KernelEvents::RESPONSE, array($this, 'redirectUser'));
    }
   
    public function redirectUser(FilterResponseEvent $event)
    {        
        if($this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN'))
            $response = new RedirectResponse($this->router->generate('brewnshare_ingredient'));
        else
            $response = new RedirectResponse($this->router->generate('brewnshare_homepage'));
        
        $event->setResponse($response);
    }
}