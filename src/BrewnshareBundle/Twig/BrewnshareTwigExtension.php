<?php

namespace BrewnshareBundle\Twig;

use Symfony\Component\Translation\DataCollectorTranslator;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

use BrewnshareBundle\Entity\User;

/**
 * Custom Twig extension
 *
 * @author kevinriegel
 */
class BrewnshareTwigExtension extends \Twig_Extension {
    
    private $translator;
    private $uploaderHelper;
    
    public function __construct(DataCollectorTranslator $translator, UploaderHelper $uploaderHelper) {
        $this->translator = $translator;
        $this->uploaderHelper = $uploaderHelper;
    }
    
    public function getFunctions() {
        return array(
            new \Twig_SimpleFunction('get_avatar_url', array($this, 'getAvatarUrl')),
        );
    }
    
    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('duration_humanize', array($this, 'durationHumanize')),
        );
    }
    
    public function getAvatarUrl(User $user) {
        $path = $this->uploaderHelper->asset($user, 'avatar');
        
        if(is_null($path))
            $path = $user->getGender() == "F" ? "/assets/img/user-f.png" : "/assets/img/user-m.png";
        
        return $path;
    }

    public function durationHumanize($duration) {
        // Less than 2 hours, display in minutes
        if($duration < 120)
            return $duration.' '.$this->translator->trans($duration <= 1 ? "minute" : "minutes");
        // Between 2 and 24 hours, display in hours
        elseif($duration >= 120 && $duration < 1440)
            return round($duration/60).' '.$this->translator->trans("hours");
        // More than 24 hours, display in days
        else
            return round($duration/(60*24)).' '.$this->translator->trans(round($duration/(60*24)) <= 1 ? 'day' : 'days');
    }


    public function getName() {
        return 'brewnshare_twig_extension';
    }
}
