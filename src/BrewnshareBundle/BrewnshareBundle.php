<?php

namespace BrewnshareBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BrewnshareBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
