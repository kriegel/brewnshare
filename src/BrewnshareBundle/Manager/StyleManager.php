<?php

namespace BrewnshareBundle\Manager;

use Doctrine\ORM\EntityManager;

use BrewnshareBundle\Entity\Style;
use BrewnshareBundle\Entity\StyleTranslation;

class StyleManager {
    
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    public function getRepository()
    {
        return $this->em->getRepository('BrewnshareBundle:Style');
    }
    
    /**
     * Get from database or create Style entity for $style xml element
     * 
     * @param SimpleXMLElement $style
     * @param string $locale
     * @param bool $update
     * 
     * @return Ingredient
     */
    public function getStyleObjectFromXMl($style, $locale = 'fr', $update = false) {
        // Looking for fermentable entity on $locale locale
        $oStyle = $this->getRepository()->findOneByLocaleName($locale, (string)$style->NAME);
        
        // If not found on $locale, looking for name in other translations available
        if(!$oStyle) {
            $oStyle = $this->getRepository()->findOneByName((string)$style->NAME);
            
            // If style is found threw an other translation, forcing update to set values for $locale translation
            if($oStyle) {
                $oStyle->translate($locale, false)->setName((string)$style->NAME);
                $update = true;
            } 
        }
        
        // If Style is still not found, then we create it
        if(!$oStyle) {
            $oStyle = new Style();
            $oStyle->translate($locale, false)->setName((string)$style->NAME);
//            $oStyle->setValidated(true);
            
            $this->em->persist($oStyle);
        }
        
        // On object creation (id is null) or if we force update
        if(is_null($oStyle->getId()) || $update === true) {
            $oStyle->setVersion((int)$style->VERSION);
            $oStyle->translate($locale)->setCategory((string)$style->CATEGORY);
            $oStyle->setCategoryNumber((int)$style->CATEGORY_NUMBER);
            $oStyle->setStyleLetter((string)$style->STYLE_LETTER);
            $oStyle->setStyleGuide((string)$style->STYLE_GUIDE);
            $oStyle->setType((string)$style->TYPE);
            $oStyle->setOGMin((float)$style->OG_MIN);
            $oStyle->setOGMax((float)$style->OG_MAX);
            $oStyle->setFGMin((float)$style->FG_MIN);
            $oStyle->setFGMax((float)$style->FG_MAX);
            $oStyle->setIBUMin((float)$style->IBU_MIN);
            $oStyle->setIBUMax((float)$style->IBU_MAX);
            $oStyle->setColorMin((float)$style->COLOR_MIN);
            $oStyle->setColorMax((float)$style->COLOR_MAX);
            $oStyle->setCarbMin((float)$style->CARB_MIN);
            $oStyle->setCarbMax((float)$style->CARB_MAX);
            $oStyle->setAbvMin((float)$style->ABV_MIN);
            $oStyle->setAbvMax((float)$style->ABV_MAX);
            $oStyle->translate($locale)->setNotes((string)$style->NOTES);
            $oStyle->translate($locale)->setProfile((string)$style->PROFILE);
            $oStyle->translate($locale)->setIngredients((string)$style->INGREDIENTS);
            $oStyle->translate($locale)->setExamples((string)$style->EXAMPLES);
            
            // In order to persist new translations, call mergeNewTranslations method, before flush
            $oStyle->mergeNewTranslations();
            
            $this->em->flush();
        }
        
        return $oStyle;
    }
}