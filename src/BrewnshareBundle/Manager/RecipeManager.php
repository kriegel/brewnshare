<?php

namespace BrewnshareBundle\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Filesystem\Filesystem;

use BrewnshareBundle\Manager\IngredientManager;
use BrewnshareBundle\Manager\StyleManager;
use BrewnshareBundle\Util\XmlTools;

use BrewnshareBundle\Entity\Recipe;
use BrewnshareBundle\Entity\RecipeHasIngredient;
use BrewnshareBundle\Entity\Style;
use BrewnshareBundle\Tools\IngredientType;

class RecipeManager {
    
    protected $em;
    protected $ingredientManager;
    protected $styleManager;

    public function __construct(EntityManager $em, IngredientManager $ingredientManager, StyleManager $styleManager)
    {
        $this->em = $em;
        $this->ingredientManager = $ingredientManager;
        $this->styleManager = $styleManager;
    }
    
    public function getRepository()
    {
        return $this->em->getRepository('BrewnshareBundle:Recipe');
    }
    
    public function importFromXmlFile(Recipe &$recipe, $file, $locale = 'fr') {
        $fs = new Filesystem();
        
        if($fs->exists($file)) {
            $this->em->persist($recipe);

            $xml = XmlTools::loadXml($file);
            $xmlRecipe = $xml->RECIPE;
            $xmlFermentables = $xmlRecipe->FERMENTABLES;
            $xmlHops = $xmlRecipe->HOPS;
            $xmlYeasts = $xmlRecipe->YEASTS;
            $xmlMiscs = $xmlRecipe->MISCS;
            
            /**
             * Recipe values
             */
            $recipe->setName((string)$xmlRecipe->NAME);
            $recipe->setVersion((float)$xmlRecipe->VERSION);
            $recipe->setType((string)$xmlRecipe->TYPE);
            $recipe->setBatchSize((float)$xmlRecipe->BATCH_SIZE);
            $recipe->setBoilSize((float)$xmlRecipe->BOIL_SIZE);
            $recipe->setBoilTime((float)$xmlRecipe->BOIL_TIME);
            $recipe->setEfficiency((float)$xmlRecipe->EFFICIENCY);
            $recipe->setOG((float)$xmlRecipe->OG);
            $recipe->setFG((float)$xmlRecipe->FG);
            $recipe->setNotes($xmlRecipe->NOTES);
            $recipe->setTasteNotes($xmlRecipe->TASTE_NOTES);
            $abv = explode(' %', (string)$xmlRecipe->ABV);
            if(!empty($abv))
                $recipe->setAbv((float)$abv[0]);
            $ibu = explode(' IBUs', (string)$xmlRecipe->IBU);
            if(!empty($ibu))
                $recipe->setBitterness((int)round((float)$ibu[0]));
            $ebc = explode(' EBC', (string)$xmlRecipe->EST_COLOR);
            if(!empty($ebc))
                $recipe->setColor((int)round((float)$ebc[0]));
            
            /**
             * Fermentables import
             */
            foreach($xmlFermentables->FERMENTABLE as $fermentable) {
                
                if(empty($fermentable->NAME))
                    continue;
                
                $oFermentable = $this->ingredientManager->getFermentableObjectFromXMl($fermentable, $locale);
                
                $rhi = new RecipeHasIngredient();
                $rhi->setRecipe($recipe);
                $rhi->setIngredient($oFermentable);
                $rhi->setName((string)$fermentable->NAME);
                $rhi->setQuantity(round((float)$fermentable->AMOUNT, 3));
                $rhi->setAddAfterBoil($fermentable->ADD_AFTER_BOIL == 'TRUE');
                $rhi->setMainType(IngredientType::CONSTANT_TYPE_FERMENTABLE);
                
                $this->em->persist($rhi);
            }
            
            /**
             * Hops import
             */
            foreach($xmlHops->HOP as $hop) {
                
                if(empty($hop->NAME))
                    continue;
                
                $oHop = $this->ingredientManager->getHopObjectFromXMl($hop, $locale);
                
                $rhi = new RecipeHasIngredient();
                $rhi->setRecipe($recipe);
                $rhi->setIngredient($oHop);
                $rhi->setName((string)$hop->NAME);
                $rhi->setQuantity(round((float)$hop->AMOUNT, 3));
                $rhi->setUse((string)$hop->USE);
                $rhi->setTime((float)$hop->TIME);
                $rhi->setForm((string)$hop->FORM);
                $rhi->setAlpha((float)$hop->ALPHA);
                $rhi->setMainType(IngredientType::CONSTANT_TYPE_HOP);
                
                $this->em->persist($rhi);
            }
            
            /**
             * Yeasts import
             */
            foreach($xmlYeasts->YEAST as $yeast) {
                
                if(empty($yeast->NAME))
                    continue;
                
                $oYeast = $this->ingredientManager->getYeastObjectFromXMl($yeast, $locale);
                
                $name = (string)$yeast->NAME;
                if((string)$yeast->LABORATORY || (string)$yeast->PRODUCT_ID) {
                    $name .= " (";
                    if((string)$yeast->LABORATORY) $name .= (string)$yeast->LABORATORY;
                    if((string)$yeast->LABORATORY && (string)$yeast->PRODUCT_ID) $name .= " ";
                    if((string)$yeast->PRODUCT_ID) $name .= (string)$yeast->PRODUCT_ID;
                    $name .= ")";
                }
                $rhi = new RecipeHasIngredient();
                $rhi->setRecipe($recipe);
                $rhi->setIngredient($oYeast);
                $rhi->setName($name);
                $rhi->setAddToSecondary($yeast->ADD_TO_SECONDARY == 'TRUE');
                $rhi->setMainType(IngredientType::CONSTANT_TYPE_YEAST);
                
                $this->em->persist($rhi);
            }
            
            /**
             * Miscs import
             */
            foreach($xmlMiscs->MISC as $misc) {
                
                if(empty($misc->NAME))
                    continue;
                
                $oMisc = $this->ingredientManager->getMiscObjectFromXMl($misc, $locale);
                
                $rhi = new RecipeHasIngredient();
                $rhi->setRecipe($recipe);
                $rhi->setIngredient($oMisc);
                $rhi->setName((string)$misc->NAME);
                $rhi->setQuantity(round((float)$misc->AMOUNT, 3));
                $rhi->setUse((string)$misc->USE);
                $rhi->setTime((float)$misc->TIME);
                $rhi->setType((string)$misc->TYPE);
                $rhi->setMainType(IngredientType::CONSTANT_TYPE_MISC);
                
                $this->em->persist($rhi);
            }
            
            /**
             * Style import
             */
            $style = $xmlRecipe->STYLE;
            if(!empty($style->NAME)) {
                
                $oStyle = $this->styleManager->getStyleObjectFromXMl($style, $locale);
                
                if($oStyle instanceof Style)
                    $recipe->setStyle($oStyle);
            }
            
            
            $this->em->flush();
        }
        else
            throw new Exception("No file found");
    }
}