<?php

namespace BrewnshareBundle\Manager;

use Doctrine\ORM\EntityManager;

use BrewnshareBundle\Entity\Ingredient;
use BrewnshareBundle\Entity\IngredientTranslation;

use BrewnshareBundle\Tools\IngredientType;

class IngredientManager {
    
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    public function getRepository()
    {
        return $this->em->getRepository('BrewnshareBundle:Ingredient');
    }
    
    /**
     * Get from database or create fermentable Ingredient entity for $fermentable xml element
     * 
     * @param SimpleXMLElement $fermentable
     * @param string $locale
     * @param bool $update
     * 
     * @return Ingredient
     */
    public function getFermentableObjectFromXMl($fermentable, $locale = 'fr', $update = false) {
        // Looking for fermentable entity on $locale locale
        $oFermentable = $this->getRepository()->findOneByLocaleName(IngredientType::CONSTANT_TYPE_FERMENTABLE, $locale, (string)$fermentable->NAME);
        
        // If not found on $locale, looking for name in other translations available
        if(!$oFermentable) {
            $oFermentable = $this->getRepository()->findOneByName(IngredientType::CONSTANT_TYPE_FERMENTABLE, (string)$fermentable->NAME);
            
            // If fermentable is found threw an other translation, forcing update to set values for $locale translation
            if($oFermentable) {
                $oFermentable->translate($locale, false)->setName((string)$fermentable->NAME);
                $update = true;
            } 
        }
        
        // If Fermentable is still not found, then we create it
        if(!$oFermentable) {
            $oFermentable = new Ingredient();
            $oFermentable->setMainType(IngredientType::CONSTANT_TYPE_FERMENTABLE);
            $oFermentable->translate($locale, false)->setName((string)$fermentable->NAME);
//            $oFermentable->setValidated(true);
            
            $this->em->persist($oFermentable);
        }
        
        // On object creation (id is null) or if we force update
        if(is_null($oFermentable->getId()) || $update === true) {
            $oFermentable->setVersion((int)$fermentable->VERSION);
            $oFermentable->setType((string)$fermentable->TYPE);
            $oFermentable->setAmount((float)$fermentable->AMOUNT);
            $oFermentable->setYield((float)$fermentable->YIELD);
            $oFermentable->setColor((float)$fermentable->COLOR);
            $oFermentable->setAddAfterBoil($fermentable->ADD_AFTER_BOIL == 'TRUE');
            $oFermentable->translate($locale)->setOrigin((string)$fermentable->ORIGIN);
            $oFermentable->translate($locale)->setSupplier((string)$fermentable->SUPPLIER);
            $oFermentable->translate($locale)->setNotes((string)$fermentable->NOTES);
            $oFermentable->setCoarseFineDiff((float)$fermentable->COARSE_FINE_DIFF);
            $oFermentable->setMoisture((float)$fermentable->MOISTURE);
            $oFermentable->setDiastaticPower((float)$fermentable->DIASTATIC_POWER);
            $oFermentable->setProtein((float)$fermentable->PROTEIN);
            $oFermentable->setMaxInBatch((float)$fermentable->MAX_IN_BATCH);
            $oFermentable->setRecommendMash($fermentable->RECOMMEND_MASH == 'TRUE');
            $oFermentable->setIbuGalPerLb((float)$fermentable->IBU_GAL_PER_LB);
            $oFermentable->setPotential((float)$fermentable->POTENTIAL);
            $oFermentable->translate($locale)->setExtractSubstitute((string)$fermentable->EXTRACT_SUBSTITUTE);
            
            // In order to persist new translations, call mergeNewTranslations method, before flush
            $oFermentable->mergeNewTranslations();
            
            $this->em->flush();
        }
        
        return $oFermentable;
    }
    
    /**
     * Get from database or create hop Ingredient entity for $hop xml element
     * 
     * @param SimpleXMLElement $hop
     * @param string $locale
     * @param bool $update
     * 
     * @return Ingredient
     */
    public function getHopObjectFromXMl($hop, $locale = 'fr', $update = false) {
        // Looking for hop entity on $locale locale
        $oHop = $this->getRepository()->findOneByLocaleName(IngredientType::CONSTANT_TYPE_HOP, $locale, (string)$hop->NAME);
        
        // If not found on $locale, looking for name in other translations available
        if(!$oHop) {
            $oHop = $this->getRepository()->findOneByName(IngredientType::CONSTANT_TYPE_HOP, (string)$hop->NAME);
            
            // If fermentable is found threw an other translation, forcing update to set values for $locale translation
            if($oHop) {
                $oHop->translate($locale, false)->setName((string)$hop->NAME);
                $update = true;
            } 
        }
        
        // If Hop is still not found, then we create it
        if(!$oHop) {
            $oHop = new Ingredient();
            $oHop->setMainType(IngredientType::CONSTANT_TYPE_HOP);
            $oHop->translate($locale, false)->setName((string)$hop->NAME);
//            $oHop->setValidated(true);
            
            $this->em->persist($oHop);
        }
        
        // On object creation (id is null) or if we force update
        if(is_null($oHop->getId()) || $update === true) {
            $oHop->setVersion((int)$hop->VERSION);
            $oHop->translate($locale)->setOrigin((string)$hop->ORIGIN);
            $oHop->setAlpha((float)$hop->ALPHA);
            $oHop->setAmount((float)$hop->AMOUNT);
            $oHop->setUse((string)$hop->USE);
            $oHop->setTime((float)$hop->TIME);
            $oHop->translate($locale)->setNotes((string)$hop->NOTES);
            $oHop->setType((string)$hop->TYPE);
            $oHop->setForm((string)$hop->FORM);
            $oHop->setBeta((float)$hop->BETA);
            $oHop->setHsi((float)$hop->HSI);
            
            // In order to persist new translations, call mergeNewTranslations method, before flush
            $oHop->mergeNewTranslations();
            
            $this->em->flush();
        }
        
        return $oHop;
    }
    
    /**
     * Get from database or create yeast Ingredient entity for $yeast xml element
     * 
     * @param SimpleXMLElement $yeast
     * @param string $locale
     * @param bool $update
     * 
     * @return Ingredient
     */
    public function getYeastObjectFromXMl($yeast, $locale = 'fr', $update = false) {
        // Looking for yeast entity on $locale locale
        $oYeast = $this->getRepository()->findOneByLocaleName(IngredientType::CONSTANT_TYPE_YEAST, $locale, (string)$yeast->NAME, (string)$yeast->LABORATOR, (string)$yeast->PRODUCT_ID);
        
        // If not found on $locale, looking for name in other translations available
        if(!$oYeast) {
            $oYeast = $this->getRepository()->findOneByName(IngredientType::CONSTANT_TYPE_YEAST, (string)$yeast->NAME, (string)$yeast->LABORATORY, (string)$yeast->PRODUCT_ID);
            
            // If fermentable is found threw an other translation, forcing update to set values for $locale translation
            if($oYeast) {
                $oYeast->translate($locale, false)->setName((string)$yeast->NAME);
                $update = true;
            } 
        }
        
        // If Yeast is still not found, then we create it
        if(!$oYeast) {
            $oYeast = new Ingredient();
            $oYeast->setMainType(IngredientType::CONSTANT_TYPE_YEAST);
            $oYeast->translate($locale, false)->setName((string)$yeast->NAME);
//            $oYeast->setValidated(true);
            
            $this->em->persist($oYeast);
        }
        
        // On object creation (id is null) or if we force update
        if(is_null($oYeast->getId()) || $update === true) {
            $oYeast->setVersion((int)$yeast->VERSION);
            $oYeast->setType((string)$yeast->TYPE);
            $oYeast->setForm((string)$yeast->FORM);
            $oYeast->setAmount((float)$yeast->AMOUNT);
            $oYeast->setAmountIsWeight($yeast->AMOUNT_IS_WEIGHT == 'TRUE');
            $oYeast->setLaboratory((string)$yeast->LABORATORY);
            $oYeast->setProductId((string)$yeast->PRODUCT_ID);
            $oYeast->setMinTemperature((float)$yeast->MIN_TEMPERATURE);
            $oYeast->setMaxTemperature((float)$yeast->MAX_TEMPERATURE);
            $oYeast->setFlocculation((string)$yeast->FLOCCULATION);
            $oYeast->setAttenuation((float)$yeast->ATTENUATION);
            $oYeast->translate($locale)->setNotes((string)$yeast->NOTES);
            $oYeast->translate($locale)->setBestFor((string)$yeast->BEST_FOR);
            $oYeast->setMaxReuse((int)$yeast->MAX_REUSE);
            $oYeast->setAddToSecondary($yeast->ADD_TO_SECONDARY == 'TRUE');
            
            // In order to persist new translations, call mergeNewTranslations method, before flush
            $oYeast->mergeNewTranslations();
            
            $this->em->flush();
        }
        
        return $oYeast;
    }
    
    /**
     * Get from database or create miscellaneous Ingredient entity for $yeast xml element
     * 
     * @param SimpleXMLElement $misc
     * @param string $locale
     * @param bool $update
     * 
     * @return Ingredient
     */
    public function getMiscObjectFromXMl($misc, $locale = 'fr', $update = false) {
        // Looking for yeast entity on $locale locale
        $oMisc = $this->getRepository()->findOneByLocaleName(IngredientType::CONSTANT_TYPE_MISC, $locale, (string)$misc->NAME);
        
        // If not found on $locale, looking for name in other translations available
        if(!$oMisc) {
            $oMisc = $this->getRepository()->findOneByName(IngredientType::CONSTANT_TYPE_MISC, (string)$misc->NAME);
            
            // If fermentable is found threw an other translation, forcing update to set values for $locale translation
            if($oMisc) {
                $oMisc->translate($locale, false)->setName((string)$misc->NAME);
                $update = true;
            } 
        }
        
        // If Misc is still not found, then we create it
        if(!$oMisc) {
            $oMisc = new Ingredient();
            $oMisc->setMainType(IngredientType::CONSTANT_TYPE_MISC);
            $oMisc->translate($locale, false)->setName((string)$misc->NAME);
//            $oMisc->setValidated(true);
            
            $this->em->persist($oMisc);
        }
        
        // On object creation (id is null) or if we force update
        if(is_null($oMisc->getId()) || $update === true) {
            $oMisc->setVersion((int)$misc->VERSION);
            $oMisc->setType((string)$misc->TYPE);
            $oMisc->setUse((string)$misc->USE);
            $oMisc->setAmount((float)$misc->AMOUNT);
            $oMisc->setTime((float)$misc->TIME);
            $oMisc->setAmountIsWeight($misc->AMOUNT_IS_WEIGHT == 'TRUE');
            $oMisc->translate($locale)->setUseFor((string)$misc->USE_FOR);
            $oMisc->translate($locale)->setNotes((string)$misc->NOTES);
            
            // In order to persist new translations, call mergeNewTranslations method, before flush
            $oMisc->mergeNewTranslations();
            
            $this->em->flush();
        }
        
        return $oMisc;
    }
}