<?php

namespace BrewnshareBundle\Model;

use BrewnshareBundle\Entity\User;

interface UserControllerInterface
{
    public function setRouteUser(User $user);
    
    public function setDefaultUser();
    
    public function getRouteUser();
}