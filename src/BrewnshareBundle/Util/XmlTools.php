<?php

namespace BrewnshareBundle\Util;

/**
 * Methods to deal with Xml files
 *
 * @author kevinriegel
 */
class XmlTools {

    public function loadXml($file) {
        
        if(file_exists($file)) {
            $xmlTest = new \DOMDocument('1.0', 'UTF-8');
            $xmlTest->preserveWhiteSpace = false;
            $xmlTest->load($file);

            libxml_use_internal_errors(true);
//            $xmlstr = file_get_contents($file);

//            $doc = simplexml_load_string($xmlstr);
            $doc = simplexml_load_file($file);
//            $xml = explode('\n', $xmlstr);
            $errors = libxml_get_errors();

            $returnErrorXml = "";

            if(!$doc) {
                $errorXml = true;
                $returnErrorXml = "<ul>";
                foreach ($errors as $error) {
                    $returnErrorXml  .= $xml[$error->line - 1];
                    
                    switch ($error->level) {
                            case LIBXML_ERR_WARNING:
                                $returnErrorXml .= "<li>Warning $error->code : ";
                                break;
                             case LIBXML_ERR_ERROR:
                                $returnErrorXml .= "<li>Error $error->code : ";
                                break;
                            case LIBXML_ERR_FATAL:
                                $returnErrorXml .= "<li>Fatal Error $error->code : ";
                                break;
                        }
                        
                         $returnErrorXml .= trim($error->message) .
                               "-- Ligne : $error->line " .
                               "-- Colonne : $error->column </li>";
                }
                $returnErrorXml .= "</ul>";
                
                throw new \Exception($returnErrorXml);
            }
            
//            return $xmlTest;
            return $doc;
        }
        else
            throw new \Exception("File not found");
    }
}
