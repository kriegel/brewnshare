<?php

namespace BrewnshareBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="BrewnshareBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("slug")
 * @Vich\Uploadable
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     *  @ORM\Column(name="facebook_id", type="string", length=255, nullable=true)
     */
    protected $facebookId;
    
    /**
     * @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true)
     */
    protected $facebookAccessToken;
    
    /**
     *  @ORM\Column(name="google_id", type="string", length=255, nullable=true)
     */
    protected $googleId;
    
    /**
     * @ORM\Column(name="google_access_token", type="string", length=255, nullable=true)
     */
    protected $googleAccessToken;

    /**
     * @var bool
     *
     * @ORM\Column(name="terms", type="boolean", nullable=true)
     */
    private $terms = false;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 255
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 255
     * )
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=false, unique=true)
     * @Gedmo\Slug(fields={"surname", "name"}, updatable=false)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     *
     * @var string
     */
    private $gender;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthdate", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $birthdate;

    /**
     * @var array
     *
     * @ORM\Column(name="privacy", type="json_array", nullable=true)
     */
    private $privacy;
    
    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="avatar", fileNameProperty="avatarName")
     * 
     * @var File
     */
    private $avatar;

    /**
     * @ORM\Column(name="avatar_name", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $avatarName;

    /**
     * @var string
     *
     * @ORM\Column(name="biography", type="blob", nullable=true)
     */
    private $biography;

    /**
     * @var int
     *
     * @ORM\Column(name="alert_range", type="integer", nullable=true)
     * @Assert\Type(
     *     type="integer"
     * )
     */
    private $alertRange = 10;

    /**
     * @var array
     *
     * @ORM\Column(name="alert_method", type="json_array", nullable=true)
     */
    private $alertMethod;
    
    /**
     * @ORM\OneToOne(targetEntity="Address", mappedBy="user", cascade={"persist", "remove"})
     * @Assert\Valid
     */
    private $mainAddress;
    
    /**
     * @ORM\OneToOne(targetEntity="Equipment", mappedBy="user", cascade={"persist", "remove"})
     * @Assert\Valid
     */
    private $equipment;
    
    /**
     * @ORM\OneToMany(targetEntity="Recipe", mappedBy="user", fetch="EXTRA_LAZY")
     */
    private $recipes;
    
    /**
     * @ORM\OneToMany(targetEntity="UserSubscription", mappedBy="user", fetch="EXTRA_LAZY")
     */
    private $followers;
    
    /**
     * @ORM\OneToMany(targetEntity="UserSubscription", mappedBy="subscriber", fetch="EXTRA_LAZY")
     */
    private $followings;
    
    /**
     * @ORM\OneToMany(targetEntity="UserHasDiscussion", mappedBy="user", fetch="EXTRA_LAZY")
     */
    private $discussions;
    
    /**
     * @ORM\Column(name="created_at", type="datetime")
     *
     * @var \DateTime
     */
    private $createdAt;
    
    /**
     * @ORM\Column(name="updated_at", type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;


    public function __construct()
    {
        $this->recipes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->discussions = new \Doctrine\Common\Collections\ArrayCollection();
        
        parent::__construct();
    }
    
    public function __toString() {
        return $this->surname." ".$this->name;
    }

    public function setEmail($email)
    {
        $this->setUsername($email);
        parent::setEmail($email);
    }
    
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }
    
    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set facebookId
     *
     * @param string $facebookId
     *
     * @return User
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    /**
     * Get facebookId
     *
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * Set facebookAccessToken
     *
     * @param string $facebookAccessToken
     *
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebookAccessToken = $facebookAccessToken;

        return $this;
    }

    /**
     * Get facebookAccessToken
     *
     * @return string
     */
    public function getFacebookAccessToken()
    {
        return $this->facebookAccessToken;
    }

    /**
     * Set googleId
     *
     * @param string $googleId
     *
     * @return User
     */
    public function setGoogleId($googleId)
    {
        $this->googleId = $googleId;

        return $this;
    }

    /**
     * Get googleId
     *
     * @return string
     */
    public function getGoogleId()
    {
        return $this->googleId;
    }

    /**
     * Set googleAccessToken
     *
     * @param string $googleAccessToken
     *
     * @return User
     */
    public function setGoogleAccessToken($googleAccessToken)
    {
        $this->googleAccessToken = $googleAccessToken;

        return $this;
    }

    /**
     * Get googleAccessToken
     *
     * @return string
     */
    public function getGoogleAccessToken()
    {
        return $this->googleAccessToken;
    }

    /**
     * Set terms
     *
     * @param boolean $terms
     *
     * @return User
     */
    public function setTerms($terms)
    {
        $this->terms = $terms;

        return $this;
    }

    /**
     * Get terms
     *
     * @return boolean
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return User
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return User
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     *
     * @return User
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set privacy
     *
     * @param array $privacy
     *
     * @return User
     */
    public function setPrivacy($privacy)
    {
        $this->privacy = $privacy;

        return $this;
    }

    /**
     * Get privacy
     *
     * @return array
     */
    public function getPrivacy()
    {
        return $this->privacy;
    }
    
    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $avatar
     *
     * @return User
     */
    public function setAvatar(File $avatar = null)
    {
        $this->avatar = $avatar;

        if ($avatar) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return File|null
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set avatarName
     *
     * @param string $avatarName
     *
     * @return User
     */
    public function setAvatarName($avatarName)
    {
        $this->avatarName = $avatarName;

        return $this;
    }

    /**
     * Get avatarName
     *
     * @return string
     */
    public function getAvatarName()
    {
        return $this->avatarName;
    }

    /**
     * Set biography
     *
     * @param string $biography
     *
     * @return User
     */
    public function setBiography($biography)
    {
        $this->biography = $biography;

        return $this;
    }

    /**
     * Get biography
     *
     * @return string
     */
    public function getBiography()
    {
        return $this->biography;
    }

    /**
     * Set alertRange
     *
     * @param integer $alertRange
     *
     * @return User
     */
    public function setAlertRange($alertRange)
    {
        $this->alertRange = $alertRange;

        return $this;
    }

    /**
     * Get alertRange
     *
     * @return int
     */
    public function getAlertRange()
    {
        return $this->alertRange;
    }

    /**
     * Set alertMethod
     *
     * @param array $alertMethod
     *
     * @return User
     */
    public function setAlertMethod($alertMethod)
    {
        $this->alertMethod = $alertMethod;

        return $this;
    }

    /**
     * Get alertMethod
     *
     * @return array
     */
    public function getAlertMethod()
    {
        return $this->alertMethod;
    }

    /**
     * Set mainAddress
     *
     * @param \BrewnshareBundle\Entity\Address $mainAddress
     *
     * @return User
     */
    public function setMainAddress(\BrewnshareBundle\Entity\Address $mainAddress = null)
    {
        $this->mainAddress = $mainAddress;

        return $this;
    }

    /**
     * Get mainAddress
     *
     * @return \BrewnshareBundle\Entity\Address
     */
    public function getMainAddress()
    {
        return $this->mainAddress;
    }

    /**
     * Set equipment
     *
     * @param \BrewnshareBundle\Entity\Equipment $equipment
     *
     * @return User
     */
    public function setEquipment(\BrewnshareBundle\Entity\Equipment $equipment = null)
    {
        $this->equipment = $equipment;

        return $this;
    }

    /**
     * Get equipment
     *
     * @return \BrewnshareBundle\Entity\Equipment
     */
    public function getEquipment()
    {
        return $this->equipment;
    }

    /**
     * Add recipe
     *
     * @param \BrewnshareBundle\Entity\Recipe $recipe
     *
     * @return User
     */
    public function addRecipe(\BrewnshareBundle\Entity\Recipe $recipe)
    {
        $this->recipes[] = $recipe;

        return $this;
    }

    /**
     * Remove recipe
     *
     * @param \BrewnshareBundle\Entity\Recipe $recipe
     */
    public function removeRecipe(\BrewnshareBundle\Entity\Recipe $recipe)
    {
        $this->recipes->removeElement($recipe);
    }

    /**
     * Get recipes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecipes()
    {
        return $this->recipes;
    }

    /**
     * Add follower
     *
     * @param \BrewnshareBundle\Entity\UserSubscription $follower
     *
     * @return User
     */
    public function addFollower(\BrewnshareBundle\Entity\UserSubscription $follower)
    {
        $this->followers[] = $follower;

        return $this;
    }

    /**
     * Remove follower
     *
     * @param \BrewnshareBundle\Entity\UserSubscription $follower
     */
    public function removeFollower(\BrewnshareBundle\Entity\UserSubscription $follower)
    {
        $this->followers->removeElement($follower);
    }

    /**
     * Get followers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * Add following
     *
     * @param \BrewnshareBundle\Entity\UserSubscription $following
     *
     * @return User
     */
    public function addFollowing(\BrewnshareBundle\Entity\UserSubscription $following)
    {
        $this->followings[] = $following;

        return $this;
    }

    /**
     * Remove following
     *
     * @param \BrewnshareBundle\Entity\UserSubscription $following
     */
    public function removeFollowing(\BrewnshareBundle\Entity\UserSubscription $following)
    {
        $this->followings->removeElement($following);
    }

    /**
     * Get followings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowings()
    {
        return $this->followings;
    }

    /**
     * Add discussion
     *
     * @param \BrewnshareBundle\Entity\UserHasDiscussion $discussion
     *
     * @return User
     */
    public function addDiscussion(\BrewnshareBundle\Entity\UserHasDiscussion $discussion)
    {
        $this->discussions[] = $discussion;

        return $this;
    }

    /**
     * Remove discussion
     *
     * @param \BrewnshareBundle\Entity\UserHasDiscussion $discussion
     */
    public function removeDiscussion(\BrewnshareBundle\Entity\UserHasDiscussion $discussion)
    {
        $this->discussions->removeElement($discussion);
    }

    /**
     * Get discussions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiscussions()
    {
        return $this->discussions;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
