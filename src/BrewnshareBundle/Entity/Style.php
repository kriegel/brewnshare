<?php

namespace BrewnshareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Style
 *
 * @ORM\Table(name="style")
 * @ORM\Entity(repositoryClass="BrewnshareBundle\Repository\StyleRepository")
 */
class Style
{
    use ORMBehaviors\Translatable\Translatable;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="version", type="integer", nullable=true)
     * @Assert\Type(
     *     type="integer"
     * )
     */
    private $version;

    /**
     * @var int
     *
     * @ORM\Column(name="category_number", type="integer", nullable=true)
     * @Assert\Type(
     *     type="integer"
     * )
     */
    private $categoryNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="style_letter", type="string", length=3, nullable=true)
     * @Assert\Length(
     *      max = 3
     * )
     */
    private $styleLetter;

    /**
     * @var string
     *
     * @ORM\Column(name="style_guide", type="string", length=127, nullable=true)
     * @Assert\Length(
     *      max = 127
     * )
     */
    private $styleGuide;

    /**
     * @var string
     *
     * @ORM\Column(name="style_type", type="string", length=127, nullable=true)
     * @Assert\Length(
     *      max = 127
     * )
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(name="og_min", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $OGMin;

    /**
     * @var float
     *
     * @ORM\Column(name="og_max", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $OGMax;

    /**
     * @var float
     *
     * @ORM\Column(name="fg_min", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $FGMin;

    /**
     * @var float
     *
     * @ORM\Column(name="fg_max", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $FGMax;

    /**
     * @var int
     *
     * @ORM\Column(name="ibu_min", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $IBUMin;

    /**
     * @var int
     *
     * @ORM\Column(name="ibu_max", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $IBUMax;

    /**
     * @var int
     *
     * @ORM\Column(name="color_min", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $colorMin;

    /**
     * @var int
     *
     * @ORM\Column(name="color_max", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $colorMax;

    /**
     * @var int
     *
     * @ORM\Column(name="carb_min", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $carbMin;

    /**
     * @var int
     *
     * @ORM\Column(name="carb_max", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $carbMax;

    /**
     * @var int
     *
     * @ORM\Column(name="abv_min", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $abvMin;

    /**
     * @var int
     *
     * @ORM\Column(name="abv_max", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $abvMax;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="validated", type="boolean", nullable=false)
     */
    private $validated = false;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set version
     *
     * @param integer $version
     *
     * @return Style
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return integer
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set categoryNumber
     *
     * @param integer $categoryNumber
     *
     * @return Style
     */
    public function setCategoryNumber($categoryNumber)
    {
        $this->categoryNumber = $categoryNumber;

        return $this;
    }

    /**
     * Get categoryNumber
     *
     * @return integer
     */
    public function getCategoryNumber()
    {
        return $this->categoryNumber;
    }

    /**
     * Set styleLetter
     *
     * @param string $styleLetter
     *
     * @return Style
     */
    public function setStyleLetter($styleLetter)
    {
        $this->styleLetter = $styleLetter;

        return $this;
    }

    /**
     * Get styleLetter
     *
     * @return string
     */
    public function getStyleLetter()
    {
        return $this->styleLetter;
    }

    /**
     * Set styleGuide
     *
     * @param string $styleGuide
     *
     * @return Style
     */
    public function setStyleGuide($styleGuide)
    {
        $this->styleGuide = $styleGuide;

        return $this;
    }

    /**
     * Get styleGuide
     *
     * @return string
     */
    public function getStyleGuide()
    {
        return $this->styleGuide;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Style
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set oGMin
     *
     * @param float $oGMin
     *
     * @return Style
     */
    public function setOGMin($oGMin)
    {
        $this->OGMin = $oGMin;

        return $this;
    }

    /**
     * Get oGMin
     *
     * @return float
     */
    public function getOGMin()
    {
        return $this->OGMin;
    }

    /**
     * Set oGMax
     *
     * @param float $oGMax
     *
     * @return Style
     */
    public function setOGMax($oGMax)
    {
        $this->OGMax = $oGMax;

        return $this;
    }

    /**
     * Get oGMax
     *
     * @return float
     */
    public function getOGMax()
    {
        return $this->OGMax;
    }

    /**
     * Set fGMin
     *
     * @param float $fGMin
     *
     * @return Style
     */
    public function setFGMin($fGMin)
    {
        $this->FGMin = $fGMin;

        return $this;
    }

    /**
     * Get fGMin
     *
     * @return float
     */
    public function getFGMin()
    {
        return $this->FGMin;
    }

    /**
     * Set fGMax
     *
     * @param float $fGMax
     *
     * @return Style
     */
    public function setFGMax($fGMax)
    {
        $this->FGMax = $fGMax;

        return $this;
    }

    /**
     * Get fGMax
     *
     * @return float
     */
    public function getFGMax()
    {
        return $this->FGMax;
    }

    /**
     * Set iBUMin
     *
     * @param float $iBUMin
     *
     * @return Style
     */
    public function setIBUMin($iBUMin)
    {
        $this->IBUMin = $iBUMin;

        return $this;
    }

    /**
     * Get iBUMin
     *
     * @return float
     */
    public function getIBUMin()
    {
        return $this->IBUMin;
    }

    /**
     * Set iBUMax
     *
     * @param float $iBUMax
     *
     * @return Style
     */
    public function setIBUMax($iBUMax)
    {
        $this->IBUMax = $iBUMax;

        return $this;
    }

    /**
     * Get iBUMax
     *
     * @return float
     */
    public function getIBUMax()
    {
        return $this->IBUMax;
    }

    /**
     * Set colorMin
     *
     * @param float $colorMin
     *
     * @return Style
     */
    public function setColorMin($colorMin)
    {
        $this->colorMin = $colorMin;

        return $this;
    }

    /**
     * Get colorMin
     *
     * @return float
     */
    public function getColorMin()
    {
        return $this->colorMin;
    }

    /**
     * Set colorMax
     *
     * @param float $colorMax
     *
     * @return Style
     */
    public function setColorMax($colorMax)
    {
        $this->colorMax = $colorMax;

        return $this;
    }

    /**
     * Get colorMax
     *
     * @return float
     */
    public function getColorMax()
    {
        return $this->colorMax;
    }

    /**
     * Set carbMin
     *
     * @param float $carbMin
     *
     * @return Style
     */
    public function setCarbMin($carbMin)
    {
        $this->carbMin = $carbMin;

        return $this;
    }

    /**
     * Get carbMin
     *
     * @return float
     */
    public function getCarbMin()
    {
        return $this->carbMin;
    }

    /**
     * Set carbMax
     *
     * @param float $carbMax
     *
     * @return Style
     */
    public function setCarbMax($carbMax)
    {
        $this->carbMax = $carbMax;

        return $this;
    }

    /**
     * Get carbMax
     *
     * @return float
     */
    public function getCarbMax()
    {
        return $this->carbMax;
    }

    /**
     * Set abvMin
     *
     * @param float $abvMin
     *
     * @return Style
     */
    public function setAbvMin($abvMin)
    {
        $this->abvMin = $abvMin;

        return $this;
    }

    /**
     * Get abvMin
     *
     * @return float
     */
    public function getAbvMin()
    {
        return $this->abvMin;
    }

    /**
     * Set abvMax
     *
     * @param float $abvMax
     *
     * @return Style
     */
    public function setAbvMax($abvMax)
    {
        $this->abvMax = $abvMax;

        return $this;
    }

    /**
     * Get abvMax
     *
     * @return float
     */
    public function getAbvMax()
    {
        return $this->abvMax;
    }

    /**
     * Set validated
     *
     * @param boolean $validated
     *
     * @return Style
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return boolean
     */
    public function getValidated()
    {
        return $this->validated;
    }
}
