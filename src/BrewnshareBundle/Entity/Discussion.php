<?php

namespace BrewnshareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Discussion
 *
 * @ORM\Table(name="discussion")
 * @ORM\Entity(repositoryClass="BrewnshareBundle\Repository\DiscussionRepository")
 */
class Discussion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $title;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="creator_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $creator;
    
    /**
     * @ORM\OneToMany(targetEntity="UserHasDiscussion", mappedBy="discussion")
     */
    private $users;
    
    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="discussion", fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $messages;
    
    /**
     * @ORM\ManyToOne(targetEntity="Event")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id", nullable=true)
     */
    private $event;
    
    /**
     * @ORM\ManyToOne(targetEntity="BeerSharing")
     * @ORM\JoinColumn(name="beer_sharing_id", referencedColumnName="id", nullable=true)
     */
    private $beerSharing;

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Discussion
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set creator
     *
     * @param \BrewnshareBundle\Entity\User $creator
     *
     * @return Discussion
     */
    public function setCreator(\BrewnshareBundle\Entity\User $creator = null)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \BrewnshareBundle\Entity\User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Add user
     *
     * @param \BrewnshareBundle\Entity\UserHasDiscussion $user
     *
     * @return Discussion
     */
    public function addUser(\BrewnshareBundle\Entity\UserHasDiscussion $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \BrewnshareBundle\Entity\UserHasDiscussion $user
     */
    public function removeUser(\BrewnshareBundle\Entity\UserHasDiscussion $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add message
     *
     * @param \BrewnshareBundle\Entity\Message $message
     *
     * @return Discussion
     */
    public function addMessage(\BrewnshareBundle\Entity\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \BrewnshareBundle\Entity\Message $message
     */
    public function removeMessage(\BrewnshareBundle\Entity\Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set event
     *
     * @param \BrewnshareBundle\Entity\Event $event
     *
     * @return Discussion
     */
    public function setEvent(\BrewnshareBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \BrewnshareBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set beerSharing
     *
     * @param \BrewnshareBundle\Entity\BeerSharing $beerSharing
     *
     * @return Discussion
     */
    public function setBeerSharing(\BrewnshareBundle\Entity\BeerSharing $beerSharing = null)
    {
        $this->beerSharing = $beerSharing;

        return $this;
    }

    /**
     * Get beerSharing
     *
     * @return \BrewnshareBundle\Entity\BeerSharing
     */
    public function getBeerSharing()
    {
        return $this->beerSharing;
    }
}
