<?php

namespace BrewnshareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Recipe
 *
 * @ORM\Table(name="recipe")
 * @ORM\Entity(repositoryClass="BrewnshareBundle\Repository\RecipeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Recipe
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="recipes")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=false, unique=false)
     * @Gedmo\Slug(fields={"name"}, unique_base="user", updatable=false)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $slug;

    /**
     * @var bool
     *
     * @ORM\Column(name="public", type="boolean", nullable=true)
     */
    private $public = true;

    /**
     * @var int
     *
     * @ORM\Column(name="version", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $version;

    /**
     * @var string
     *
     * @ORM\Column(name="recipe_type", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(name="batch_size", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $batchSize;

    /**
     * @var float
     *
     * @ORM\Column(name="boil_size", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $boilSize;

    /**
     * @var float
     *
     * @ORM\Column(name="boil_time", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $boilTime;

    /**
     * @var float
     *
     * @ORM\Column(name="efficiency", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $efficiency;

    /**
     * @var float
     *
     * @ORM\Column(name="abv", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $abv;

    /**
     * @var float
     *
     * @ORM\Column(name="og", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $OG;

    /**
     * @var string
     *
     * @ORM\Column(name="og_unit", type="string", length=20, nullable=true)
     * @Assert\Length(
     *      max = 20
     * )
     */
    private $OGUnit = 'SG';

    /**
     * @var float
     *
     * @ORM\Column(name="fg", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $FG;

    /**
     * @var string
     *
     * @ORM\Column(name="fg_unit", type="string", length=20, nullable=true)
     * @Assert\Length(
     *      max = 20
     * )
     */
    private $FGUnit = 'SG';

    /**
     * @var int
     *
     * @ORM\Column(name="bitterness", type="integer", nullable=true)
     * @Assert\Type(
     *     type="integer"
     * )
     */
    private $bitterness;

    /**
     * @var string
     *
     * @ORM\Column(name="bitterness_unit", type="string", length=20, nullable=true)
     * @Assert\Length(
     *      max = 20
     * )
     */
    private $bitternessUnit = 'IBU';

    /**
     * @var int
     *
     * @ORM\Column(name="color", type="integer", nullable=true)
     * @Assert\Type(
     *     type="integer"
     * )
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="color_unit", type="string", length=20, nullable=true)
     * @Assert\Length(
     *      max = 20
     * )
     */
    private $colorUnit = 'EBC';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var string
     *
     * @ORM\Column(name="taste_notes", type="text", nullable=true)
     */
    private $tasteNotes;
    
    /**
     * @ORM\ManyToOne(targetEntity="Style")
     * @ORM\JoinColumn(name="style_id", referencedColumnName="id", nullable=true)
     */
    private $style;
    
    /**
     * @ORM\OneToMany(targetEntity="RecipeHasIngredient", mappedBy="recipe", cascade={"remove"}, fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $ingredients;

    /**
     * @var string
     *
     * @ORM\Column(name="awards", type="text", nullable=true)
     */
    private $awards;
    
    /**
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="recipe", fetch="EXTRA_LAZY")
     */
    private $photos;
    
    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="recipe", fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $comments;
    
    /**
     * @ORM\ManyToMany(targetEntity="User")
     * @ORM\JoinTable(name="brewer_partners",
     *      joinColumns={@ORM\JoinColumn(name="recipe_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *  )
     */
    private $brewerPartners;
    
    /**
     * @ORM\OneToOne(targetEntity="BeerSharing", mappedBy="recipe")
     */
    private $beerSharing;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="brew_date", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $brewDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="bottled_date", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $bottledDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $updatedAt;

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ingredients = new \Doctrine\Common\Collections\ArrayCollection();
        $this->photos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->brewerPartners = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }
    
    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \BrewnshareBundle\Entity\User $user
     *
     * @return Recipe
     */
    public function setUser(\BrewnshareBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BrewnshareBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Recipe
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Recipe
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set public
     *
     * @param boolean $public
     *
     * @return Recipe
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Get public
     *
     * @return bool
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Set version
     *
     * @param float $version
     *
     * @return Recipe
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return float
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Recipe
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set batchSize
     *
     * @param float $batchSize
     *
     * @return Recipe
     */
    public function setBatchSize($batchSize)
    {
        $this->batchSize = $batchSize;

        return $this;
    }

    /**
     * Get batchSize
     *
     * @return float
     */
    public function getBatchSize()
    {
        return $this->batchSize;
    }

    /**
     * Set boilSize
     *
     * @param float $boilSize
     *
     * @return Recipe
     */
    public function setBoilSize($boilSize)
    {
        $this->boilSize = $boilSize;

        return $this;
    }

    /**
     * Get boilSize
     *
     * @return float
     */
    public function getBoilSize()
    {
        return $this->boilSize;
    }

    /**
     * Set boilTime
     *
     * @param float $boilTime
     *
     * @return Recipe
     */
    public function setBoilTime($boilTime)
    {
        $this->boilTime = $boilTime;

        return $this;
    }

    /**
     * Get boilTime
     *
     * @return float
     */
    public function getBoilTime()
    {
        return $this->boilTime;
    }

    /**
     * Set efficiency
     *
     * @param float $efficiency
     *
     * @return Recipe
     */
    public function setEfficiency($efficiency)
    {
        $this->efficiency = $efficiency;

        return $this;
    }

    /**
     * Get efficiency
     *
     * @return float
     */
    public function getEfficiency()
    {
        return $this->efficiency;
    }

    /**
     * Set abv
     *
     * @param float $abv
     *
     * @return Recipe
     */
    public function setAbv($abv)
    {
        $this->abv = $abv;

        return $this;
    }

    /**
     * Get abv
     *
     * @return float
     */
    public function getAbv()
    {
        return $this->abv;
    }

    /**
     * Set oG
     *
     * @param float $oG
     *
     * @return Recipe
     */
    public function setOG($oG)
    {
        $this->OG = $oG;

        return $this;
    }

    /**
     * Get oG
     *
     * @return float
     */
    public function getOG()
    {
        return $this->OG;
    }

    /**
     * Set oGUnit
     *
     * @param string $oGUnit
     *
     * @return Recipe
     */
    public function setOGUnit($oGUnit)
    {
        $this->OGUnit = $oGUnit;

        return $this;
    }

    /**
     * Get oGUnit
     *
     * @return string
     */
    public function getOGUnit()
    {
        return $this->OGUnit;
    }

    /**
     * Set fG
     *
     * @param float $fG
     *
     * @return Recipe
     */
    public function setFG($fG)
    {
        $this->FG = $fG;

        return $this;
    }

    /**
     * Get fG
     *
     * @return float
     */
    public function getFG()
    {
        return $this->FG;
    }

    /**
     * Set fGUnit
     *
     * @param string $fGUnit
     *
     * @return Recipe
     */
    public function setFGUnit($fGUnit)
    {
        $this->FGUnit = $fGUnit;

        return $this;
    }

    /**
     * Get fGUnit
     *
     * @return string
     */
    public function getFGUnit()
    {
        return $this->FGUnit;
    }

    /**
     * Set bitterness
     *
     * @param integer $bitterness
     *
     * @return Recipe
     */
    public function setBitterness($bitterness)
    {
        $this->bitterness = $bitterness;

        return $this;
    }

    /**
     * Get bitterness
     *
     * @return integer
     */
    public function getBitterness()
    {
        return $this->bitterness;
    }

    /**
     * Set bitternessUnit
     *
     * @param string $bitternessUnit
     *
     * @return Recipe
     */
    public function setBitternessUnit($bitternessUnit)
    {
        $this->bitternessUnit = $bitternessUnit;

        return $this;
    }

    /**
     * Get bitternessUnit
     *
     * @return string
     */
    public function getBitternessUnit()
    {
        return $this->bitternessUnit;
    }

    /**
     * Set color
     *
     * @param integer $color
     *
     * @return Recipe
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return integer
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set colorUnit
     *
     * @param string $colorUnit
     *
     * @return Recipe
     */
    public function setColorUnit($colorUnit)
    {
        $this->colorUnit = $colorUnit;

        return $this;
    }

    /**
     * Get colorUnit
     *
     * @return string
     */
    public function getColorUnit()
    {
        return $this->colorUnit;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Recipe
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return Recipe
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set tasteNotes
     *
     * @param string $tasteNotes
     *
     * @return Recipe
     */
    public function setTasteNotes($tasteNotes)
    {
        $this->tasteNotes = $tasteNotes;

        return $this;
    }

    /**
     * Get tasteNotes
     *
     * @return string
     */
    public function getTasteNotes()
    {
        return $this->tasteNotes;
    }

    /**
     * Set style
     *
     * @param \BrewnshareBundle\Entity\Style $style
     *
     * @return Recipe
     */
    public function setStyle(\BrewnshareBundle\Entity\Style $style = null)
    {
        $this->style = $style;

        return $this;
    }

    /**
     * Get style
     *
     * @return \BrewnshareBundle\Entity\Style
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * Add ingredient
     *
     * @param \BrewnshareBundle\Entity\RecipeHasIngredient $ingredient
     *
     * @return Recipe
     */
    public function addIngredient(\BrewnshareBundle\Entity\RecipeHasIngredient $ingredient)
    {
        $this->ingredients[] = $ingredient;

        return $this;
    }

    /**
     * Remove ingredient
     *
     * @param \BrewnshareBundle\Entity\RecipeHasIngredient $ingredient
     */
    public function removeIngredient(\BrewnshareBundle\Entity\RecipeHasIngredient $ingredient)
    {
        $this->ingredients->removeElement($ingredient);
    }

    /**
     * Get ingredients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }

    /**
     * Set awards
     *
     * @param string $awards
     *
     * @return Recipe
     */
    public function setAwards($awards)
    {
        $this->awards = $awards;

        return $this;
    }

    /**
     * Get awards
     *
     * @return string
     */
    public function getAwards()
    {
        return $this->awards;
    }

    /**
     * Add photo
     *
     * @param \BrewnshareBundle\Entity\Photo $photo
     *
     * @return Recipe
     */
    public function addPhoto(\BrewnshareBundle\Entity\Photo $photo)
    {
        $this->photos[] = $photo;

        return $this;
    }

    /**
     * Remove photo
     *
     * @param \BrewnshareBundle\Entity\Photo $photo
     */
    public function removePhoto(\BrewnshareBundle\Entity\Photo $photo)
    {
        $this->photos->removeElement($photo);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Add comment
     *
     * @param \BrewnshareBundle\Entity\Comment $comment
     *
     * @return Recipe
     */
    public function addComment(\BrewnshareBundle\Entity\Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \BrewnshareBundle\Entity\Comment $comment
     */
    public function removeComment(\BrewnshareBundle\Entity\Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Add brewerPartner
     *
     * @param \BrewnshareBundle\Entity\User $brewerPartner
     *
     * @return Recipe
     */
    public function addBrewerPartner(\BrewnshareBundle\Entity\User $brewerPartner)
    {
        $this->brewerPartners[] = $brewerPartner;

        return $this;
    }

    /**
     * Remove brewerPartner
     *
     * @param \BrewnshareBundle\Entity\User $brewerPartner
     */
    public function removeBrewerPartner(\BrewnshareBundle\Entity\User $brewerPartner)
    {
        $this->brewerPartners->removeElement($brewerPartner);
    }

    /**
     * Get brewerPartners
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBrewerPartners()
    {
        return $this->brewerPartners;
    }

    /**
     * Set beerSharing
     *
     * @param \BrewnshareBundle\Entity\BeerSharing $beerSharing
     *
     * @return Recipe
     */
    public function setBeerSharing(\BrewnshareBundle\Entity\BeerSharing $beerSharing = null)
    {
        $this->beerSharing = $beerSharing;

        return $this;
    }

    /**
     * Get beerSharing
     *
     * @return \BrewnshareBundle\Entity\BeerSharing
     */
    public function getBeerSharing()
    {
        return $this->beerSharing;
    }

    /**
     * Set brewDate
     *
     * @param \DateTime $brewDate
     *
     * @return Recipe
     */
    public function setBrewDate($brewDate)
    {
        $this->brewDate = $brewDate;

        return $this;
    }

    /**
     * Get brewDate
     *
     * @return \DateTime
     */
    public function getBrewDate()
    {
        return $this->brewDate;
    }

    /**
     * Set bottledDate
     *
     * @param \DateTime $bottledDate
     *
     * @return Recipe
     */
    public function setBottledDate($bottledDate)
    {
        $this->bottledDate = $bottledDate;

        return $this;
    }

    /**
     * Get bottledDate
     *
     * @return \DateTime
     */
    public function getBottledDate()
    {
        return $this->bottledDate;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Recipe
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Recipe
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
