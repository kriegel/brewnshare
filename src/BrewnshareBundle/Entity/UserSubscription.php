<?php

namespace BrewnshareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * UserSubscription
 *
 * @ORM\Table(name="user_subscription")
 * @ORM\Entity(repositoryClass="BrewnshareBundle\Repository\UserSubscriptionRepository")
 * @UniqueEntity(
 *    fields={"user", "subscriber"},
 *    errorPath="user",
 *    message="You already follow this user"
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class UserSubscription
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="followers")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank()
     */
    private $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="followings")
     * @ORM\JoinColumn(name="subscriber_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank()
     */
    private $subscriber;

    /**
     * @var bool
     *
     * @ORM\Column(name="alert_recipe", type="boolean", nullable=true)
     */
    private $alertRecipe = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="alert_event", type="boolean", nullable=true)
     */
    private $alertEvent = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="alert_beer_sharing", type="boolean", nullable=true)
     */
    private $alertBeerSharing = true;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $createdAt;


    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \BrewnshareBundle\Entity\User $user
     *
     * @return UserSubscription
     */
    public function setUser(\BrewnshareBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BrewnshareBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set subscriber
     *
     * @param \BrewnshareBundle\Entity\User $subscriber
     *
     * @return UserSubscription
     */
    public function setSubscriber(\BrewnshareBundle\Entity\User $subscriber)
    {
        $this->subscriber = $subscriber;

        return $this;
    }

    /**
     * Get subscriber
     *
     * @return \BrewnshareBundle\Entity\User
     */
    public function getSubscriber()
    {
        return $this->subscriber;
    }

    /**
     * Set alertRecipe
     *
     * @param boolean $alertRecipe
     *
     * @return UserSubscription
     */
    public function setAlertRecipe($alertRecipe)
    {
        $this->alertRecipe = $alertRecipe;

        return $this;
    }

    /**
     * Get alertRecipe
     *
     * @return bool
     */
    public function getAlertRecipe()
    {
        return $this->alertRecipe;
    }

    /**
     * Set alertEvent
     *
     * @param boolean $alertEvent
     *
     * @return UserSubscription
     */
    public function setAlertEvent($alertEvent)
    {
        $this->alertEvent = $alertEvent;

        return $this;
    }

    /**
     * Get alertEvent
     *
     * @return bool
     */
    public function getAlertEvent()
    {
        return $this->alertEvent;
    }

    /**
     * Set alertBeerSharing
     *
     * @param boolean $alertBeerSharing
     *
     * @return UserSubscription
     */
    public function setAlertBeerSharing($alertBeerSharing)
    {
        $this->alertBeerSharing = $alertBeerSharing;

        return $this;
    }

    /**
     * Get alertBeerSharing
     *
     * @return bool
     */
    public function getAlertBeerSharing()
    {
        return $this->alertBeerSharing;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return UserSubscription
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
