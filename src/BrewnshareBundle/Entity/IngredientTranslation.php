<?php

namespace BrewnshareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Ingredient
 *
 * @ORM\Table(name="ingredient_translation")
 * @ORM\Entity
 */
class IngredientTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="origin", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $origin;

    /**
     * @var string
     *
     * @ORM\Column(name="supplier", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $supplier;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var string
     *
     * @ORM\Column(name="extract_substitute", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $extractSubstitute;

    /**
     * @var string
     *
     * @ORM\Column(name="best_for", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $bestFor;

    /**
     * @var string
     *
     * @ORM\Column(name="use_for", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $useFor;

    
    /**
     * Set name
     *
     * @param string $name
     *
     * @return IngredientTranslation
     */
    public function setName($name)
    {
        $this->name = trim($name);

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set origin
     *
     * @param string $origin
     *
     * @return IngredientTranslation
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * Get origin
     *
     * @return string
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * Set supplier
     *
     * @param string $supplier
     *
     * @return IngredientTranslation
     */
    public function setSupplier($supplier)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return string
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return IngredientTranslation
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set extractSubstitute
     *
     * @param string $extractSubstitute
     *
     * @return IngredientTranslation
     */
    public function setExtractSubstitute($extractSubstitute)
    {
        $this->extractSubstitute = $extractSubstitute;

        return $this;
    }

    /**
     * Get extractSubstitute
     *
     * @return string
     */
    public function getExtractSubstitute()
    {
        return $this->extractSubstitute;
    }

    /**
     * Set bestFor
     *
     * @param string $bestFor
     *
     * @return IngredientTranslation
     */
    public function setBestFor($bestFor)
    {
        $this->bestFor = $bestFor;

        return $this;
    }

    /**
     * Get bestFor
     *
     * @return string
     */
    public function getBestFor()
    {
        return $this->bestFor;
    }

    /**
     * Set useFor
     *
     * @param string $useFor
     *
     * @return IngredientTranslation
     */
    public function setUseFor($useFor)
    {
        $this->useFor = $useFor;

        return $this;
    }

    /**
     * Get useFor
     *
     * @return string
     */
    public function getUseFor()
    {
        return $this->useFor;
    }
}
