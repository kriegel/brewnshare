<?php

namespace BrewnshareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * RecipeHasIngredient
 *
 * @ORM\Table(name="recipe_has_ingredient")
 * @ORM\Entity(repositoryClass="BrewnshareBundle\Repository\RecipeHasIngredientRepository")
 */
class RecipeHasIngredient
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Recipe", inversedBy="ingredients")
     * @ORM\JoinColumn(name="recipe_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $recipe;
    
    /**
     * @ORM\ManyToOne(targetEntity="Ingredient")
     * @ORM\JoinColumn(name="ingredient_id", referencedColumnName="id", nullable=true)
     */
    private $ingredient;

    /**
     * @var int
     *
     * @ORM\Column(name="main_type", type="integer", nullable=true)
     * @Assert\Type(
     *     type="integer"
     * )
     */
    private $mainType;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="quantity", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $quantity;

    /**
     * @var bool
     *
     * @ORM\Column(name="add_after_boil", type="boolean", nullable=true)
     */
    private $addAfterBoil;

    /**
     * @var bool
     *
     * @ORM\Column(name="add_to_secondary", type="boolean", nullable=true)
     */
    private $addToSecondary;

    /**
     * @var string
     *
     * @ORM\Column(name="ingredient_use", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $use;

    /**
     * @var int
     *
     * @ORM\Column(name="ingredient_time", type="integer", nullable=true)
     * @Assert\Type(
     *     type="integer"
     * )
     */
    private $time;

    /**
     * @var string
     *
     * @ORM\Column(name="ingredient_form", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $form;
    
    /**
     * @var float
     *
     * @ORM\Column(name="alpha", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $alpha;

    /**
     * @var string
     *
     * @ORM\Column(name="ingredient_type", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $type;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recipe
     *
     * @param \BrewnshareBundle\Entity\Recipe $recipe
     *
     * @return RecipeHasIngredient
     */
    public function setRecipe(\BrewnshareBundle\Entity\Recipe $recipe = null)
    {
        $this->recipe = $recipe;

        return $this;
    }

    /**
     * Get recipe
     *
     * @return \BrewnshareBundle\Entity\Recipe
     */
    public function getRecipe()
    {
        return $this->recipe;
    }

    /**
     * Set ingredient
     *
     * @param \BrewnshareBundle\Entity\Ingredient $ingredient
     *
     * @return RecipeHasIngredient
     */
    public function setIngredient(\BrewnshareBundle\Entity\Ingredient $ingredient = null)
    {
        $this->ingredient = $ingredient;

        return $this;
    }

    /**
     * Get ingredient
     *
     * @return \BrewnshareBundle\Entity\Ingredient
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }

    /**
     * Set mainType
     *
     * @param integer $mainType
     *
     * @return RecipeHasIngredient
     */
    public function setMainType($mainType)
    {
        $this->mainType = $mainType;

        return $this;
    }

    /**
     * Get mainType
     *
     * @return integer
     */
    public function getMainType()
    {
        return $this->mainType;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return RecipeHasIngredient
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set quantity
     *
     * @param float $quantity
     *
     * @return RecipeHasIngredient
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set addAfterBoil
     *
     * @param boolean $addAfterBoil
     *
     * @return RecipeHasIngredient
     */
    public function setAddAfterBoil($addAfterBoil)
    {
        $this->addAfterBoil = $addAfterBoil;

        return $this;
    }

    /**
     * Get addAfterBoil
     *
     * @return boolean
     */
    public function getAddAfterBoil()
    {
        return $this->addAfterBoil;
    }

    /**
     * Set addToSecondary
     *
     * @param boolean $addToSecondary
     *
     * @return RecipeHasIngredient
     */
    public function setAddToSecondary($addToSecondary)
    {
        $this->addToSecondary = $addToSecondary;

        return $this;
    }

    /**
     * Get addToSecondary
     *
     * @return boolean
     */
    public function getAddToSecondary()
    {
        return $this->addToSecondary;
    }

    /**
     * Set use
     *
     * @param string $use
     *
     * @return RecipeHasIngredient
     */
    public function setUse($use)
    {
        $this->use = $use;

        return $this;
    }

    /**
     * Get use
     *
     * @return string
     */
    public function getUse()
    {
        return $this->use;
    }

    /**
     * Set time
     *
     * @param integer $time
     *
     * @return RecipeHasIngredient
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return int
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set form
     *
     * @param string $form
     *
     * @return RecipeHasIngredient
     */
    public function setForm($form)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Get form
     *
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Set alpha
     *
     * @param float $alpha
     *
     * @return RecipeHasIngredient
     */
    public function setAlpha($alpha)
    {
        $this->alpha = $alpha;

        return $this;
    }

    /**
     * Get alpha
     *
     * @return float
     */
    public function getAlpha()
    {
        return $this->alpha;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return RecipeHasIngredient
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
