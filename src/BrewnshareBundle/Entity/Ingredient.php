<?php

namespace BrewnshareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Ingredient
 *
 * @ORM\Table(name="ingredient")
 * @ORM\Entity(repositoryClass="BrewnshareBundle\Repository\IngredientRepository")
 */
class Ingredient
{
    use ORMBehaviors\Translatable\Translatable;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="main_type", type="integer", nullable=true)
     * @Assert\Type(
     *     type="integer"
     * )
     */
    private $mainType;

    /**
     * @var int
     *
     * @ORM\Column(name="version", type="integer", nullable=true)
     * @Assert\Type(
     *     type="integer"
     * )
     */
    private $version;

    /**
     * @var string
     *
     * @ORM\Column(name="ingredient_type", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $amount;

    /**
     * @var float
     *
     * @ORM\Column(name="yield", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $yield;

    /**
     * @var float
     *
     * @ORM\Column(name="color", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $color;

    /**
     * @var bool
     *
     * @ORM\Column(name="add_after_boil", type="boolean", nullable=true)
     */
    private $addAfterBoil;

    /**
     * @var float
     *
     * @ORM\Column(name="coarse_fine_diff", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $coarseFineDiff;

    /**
     * @var float
     *
     * @ORM\Column(name="moisture", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $moisture;

    /**
     * @var float
     *
     * @ORM\Column(name="diastatic_power", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $diastaticPower;

    /**
     * @var float
     *
     * @ORM\Column(name="protein", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $protein;

    /**
     * @var float
     *
     * @ORM\Column(name="max_in_batch", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $maxInBatch;

    /**
     * @var bool
     *
     * @ORM\Column(name="recommend_mash", type="boolean", nullable=true)
     */
    private $recommendMash;

    /**
     * @var float
     *
     * @ORM\Column(name="ibu_gal_per_lb", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $ibuGalPerLb;

    /**
     * @var float
     *
     * @ORM\Column(name="potential", type="decimal", precision=11, scale=7, nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $potential;
    
    /**
     * @var float
     *
     * @ORM\Column(name="alpha", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $alpha;
    
    /**
     * @var float
     *
     * @ORM\Column(name="beta", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $beta;
    
    /**
     * @var float
     *
     * @ORM\Column(name="hsi", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $hsi;

    /**
     * @var string
     *
     * @ORM\Column(name="ingredient_use", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $use;
    
    /**
     * @var float
     *
     * @ORM\Column(name="time", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $time;

    /**
     * @var string
     *
     * @ORM\Column(name="ingredient_form", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $form;

    /**
     * @var bool
     *
     * @ORM\Column(name="amount_is_weight", type="boolean", nullable=true)
     */
    private $amountIsWeight;

    /**
     * @var string
     *
     * @ORM\Column(name="laboratory", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $laboratory;

    /**
     * @var string
     *
     * @ORM\Column(name="product_id", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $productId;

    /**
     * @var float
     *
     * @ORM\Column(name="min_temperature", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $minTemperature;

    /**
     * @var float
     *
     * @ORM\Column(name="max_temperature", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $maxTemperature;

    /**
     * @var string
     *
     * @ORM\Column(name="flocculation", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255
     * )
     */
    private $flocculation;
    
    /**
     * @var float
     *
     * @ORM\Column(name="attenuation", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $attenuation;

    /**
     * @var int
     *
     * @ORM\Column(name="max_reuse", type="integer", nullable=true)
     * @Assert\Type(
     *     type="integer"
     * )
     */
    private $maxReuse;

    /**
     * @var bool
     *
     * @ORM\Column(name="add_to_secondary", type="boolean", nullable=true)
     */
    private $addToSecondary;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="validated", type="boolean", nullable=false)
     */
    private $validated = false;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mainType
     *
     * @param integer $mainType
     *
     * @return Ingredient
     */
    public function setMainType($mainType)
    {
        $this->mainType = $mainType;

        return $this;
    }

    /**
     * Get mainType
     *
     * @return integer
     */
    public function getMainType()
    {
        return $this->mainType;
    }

    /**
     * Set version
     *
     * @param integer $version
     *
     * @return Ingredient
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return integer
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Ingredient
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return Ingredient
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set yield
     *
     * @param float $yield
     *
     * @return Ingredient
     */
    public function setYield($yield)
    {
        $this->yield = $yield;

        return $this;
    }

    /**
     * Get yield
     *
     * @return float
     */
    public function getYield()
    {
        return $this->yield;
    }

    /**
     * Set color
     *
     * @param float $color
     *
     * @return Ingredient
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return float
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set addAfterBoil
     *
     * @param boolean $addAfterBoil
     *
     * @return Ingredient
     */
    public function setAddAfterBoil($addAfterBoil)
    {
        $this->addAfterBoil = $addAfterBoil;

        return $this;
    }

    /**
     * Get addAfterBoil
     *
     * @return boolean
     */
    public function getAddAfterBoil()
    {
        return $this->addAfterBoil;
    }

    /**
     * Set coarseFineDiff
     *
     * @param float $coarseFineDiff
     *
     * @return Ingredient
     */
    public function setCoarseFineDiff($coarseFineDiff)
    {
        $this->coarseFineDiff = $coarseFineDiff;

        return $this;
    }

    /**
     * Get coarseFineDiff
     *
     * @return float
     */
    public function getCoarseFineDiff()
    {
        return $this->coarseFineDiff;
    }

    /**
     * Set moisture
     *
     * @param float $moisture
     *
     * @return Ingredient
     */
    public function setMoisture($moisture)
    {
        $this->moisture = $moisture;

        return $this;
    }

    /**
     * Get moisture
     *
     * @return float
     */
    public function getMoisture()
    {
        return $this->moisture;
    }

    /**
     * Set diastaticPower
     *
     * @param float $diastaticPower
     *
     * @return Ingredient
     */
    public function setDiastaticPower($diastaticPower)
    {
        $this->diastaticPower = $diastaticPower;

        return $this;
    }

    /**
     * Get diastaticPower
     *
     * @return float
     */
    public function getDiastaticPower()
    {
        return $this->diastaticPower;
    }

    /**
     * Set protein
     *
     * @param float $protein
     *
     * @return Ingredient
     */
    public function setProtein($protein)
    {
        $this->protein = $protein;

        return $this;
    }

    /**
     * Get protein
     *
     * @return float
     */
    public function getProtein()
    {
        return $this->protein;
    }

    /**
     * Set maxInBatch
     *
     * @param float $maxInBatch
     *
     * @return Ingredient
     */
    public function setMaxInBatch($maxInBatch)
    {
        $this->maxInBatch = $maxInBatch;

        return $this;
    }

    /**
     * Get maxInBatch
     *
     * @return float
     */
    public function getMaxInBatch()
    {
        return $this->maxInBatch;
    }

    /**
     * Set recommendMash
     *
     * @param boolean $recommendMash
     *
     * @return Ingredient
     */
    public function setRecommendMash($recommendMash)
    {
        $this->recommendMash = $recommendMash;

        return $this;
    }

    /**
     * Get recommendMash
     *
     * @return boolean
     */
    public function getRecommendMash()
    {
        return $this->recommendMash;
    }

    /**
     * Set ibuGalPerLb
     *
     * @param float $ibuGalPerLb
     *
     * @return Ingredient
     */
    public function setIbuGalPerLb($ibuGalPerLb)
    {
        $this->ibuGalPerLb = $ibuGalPerLb;

        return $this;
    }

    /**
     * Get ibuGalPerLb
     *
     * @return float
     */
    public function getIbuGalPerLb()
    {
        return $this->ibuGalPerLb;
    }

    /**
     * Set potential
     *
     * @param string $potential
     *
     * @return Ingredient
     */
    public function setPotential($potential)
    {
        $this->potential = $potential;

        return $this;
    }

    /**
     * Get potential
     *
     * @return string
     */
    public function getPotential()
    {
        return $this->potential;
    }

    /**
     * Set alpha
     *
     * @param float $alpha
     *
     * @return Ingredient
     */
    public function setAlpha($alpha)
    {
        $this->alpha = $alpha;

        return $this;
    }

    /**
     * Get alpha
     *
     * @return float
     */
    public function getAlpha()
    {
        return $this->alpha;
    }

    /**
     * Set beta
     *
     * @param float $beta
     *
     * @return Ingredient
     */
    public function setBeta($beta)
    {
        $this->beta = $beta;

        return $this;
    }

    /**
     * Get beta
     *
     * @return float
     */
    public function getBeta()
    {
        return $this->beta;
    }

    /**
     * Set hsi
     *
     * @param float $hsi
     *
     * @return Ingredient
     */
    public function setHsi($hsi)
    {
        $this->hsi = $hsi;

        return $this;
    }

    /**
     * Get hsi
     *
     * @return float
     */
    public function getHsi()
    {
        return $this->hsi;
    }

    /**
     * Set use
     *
     * @param string $use
     *
     * @return Ingredient
     */
    public function setUse($use)
    {
        $this->use = $use;

        return $this;
    }

    /**
     * Get use
     *
     * @return string
     */
    public function getUse()
    {
        return $this->use;
    }

    /**
     * Set time
     *
     * @param float $time
     *
     * @return Ingredient
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return float
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set form
     *
     * @param string $form
     *
     * @return Ingredient
     */
    public function setForm($form)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Get form
     *
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Set amountIsWeight
     *
     * @param boolean $amountIsWeight
     *
     * @return Ingredient
     */
    public function setAmountIsWeight($amountIsWeight)
    {
        $this->amountIsWeight = $amountIsWeight;

        return $this;
    }

    /**
     * Get amountIsWeight
     *
     * @return boolean
     */
    public function getAmountIsWeight()
    {
        return $this->amountIsWeight;
    }

    /**
     * Set laboratory
     *
     * @param string $laboratory
     *
     * @return Ingredient
     */
    public function setLaboratory($laboratory)
    {
        $this->laboratory = $laboratory;

        return $this;
    }

    /**
     * Get laboratory
     *
     * @return string
     */
    public function getLaboratory()
    {
        return $this->laboratory;
    }

    /**
     * Set productId
     *
     * @param string $productId
     *
     * @return Ingredient
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return string
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set minTemperature
     *
     * @param float $minTemperature
     *
     * @return Ingredient
     */
    public function setMinTemperature($minTemperature)
    {
        $this->minTemperature = $minTemperature;

        return $this;
    }

    /**
     * Get minTemperature
     *
     * @return float
     */
    public function getMinTemperature()
    {
        return $this->minTemperature;
    }

    /**
     * Set maxTemperature
     *
     * @param float $maxTemperature
     *
     * @return Ingredient
     */
    public function setMaxTemperature($maxTemperature)
    {
        $this->maxTemperature = $maxTemperature;

        return $this;
    }

    /**
     * Get maxTemperature
     *
     * @return float
     */
    public function getMaxTemperature()
    {
        return $this->maxTemperature;
    }

    /**
     * Set flocculation
     *
     * @param string $flocculation
     *
     * @return Ingredient
     */
    public function setFlocculation($flocculation)
    {
        $this->flocculation = $flocculation;

        return $this;
    }

    /**
     * Get flocculation
     *
     * @return string
     */
    public function getFlocculation()
    {
        return $this->flocculation;
    }

    /**
     * Set attenuation
     *
     * @param float $attenuation
     *
     * @return Ingredient
     */
    public function setAttenuation($attenuation)
    {
        $this->attenuation = $attenuation;

        return $this;
    }

    /**
     * Get attenuation
     *
     * @return float
     */
    public function getAttenuation()
    {
        return $this->attenuation;
    }

    /**
     * Set maxReuse
     *
     * @param integer $maxReuse
     *
     * @return Ingredient
     */
    public function setMaxReuse($maxReuse)
    {
        $this->maxReuse = $maxReuse;

        return $this;
    }

    /**
     * Get maxReuse
     *
     * @return integer
     */
    public function getMaxReuse()
    {
        return $this->maxReuse;
    }

    /**
     * Set addToSecondary
     *
     * @param boolean $addToSecondary
     *
     * @return Ingredient
     */
    public function setAddToSecondary($addToSecondary)
    {
        $this->addToSecondary = $addToSecondary;

        return $this;
    }

    /**
     * Get addToSecondary
     *
     * @return boolean
     */
    public function getAddToSecondary()
    {
        return $this->addToSecondary;
    }

    /**
     * Set validated
     *
     * @param boolean $validated
     *
     * @return Ingredient
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return boolean
     */
    public function isValidated()
    {
        return $this->validated;
    }
}
