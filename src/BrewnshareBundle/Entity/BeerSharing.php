<?php

namespace BrewnshareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BeerSharing
 *
 * @ORM\Table(name="beer_sharing")
 * @ORM\Entity(repositoryClass="BrewnshareBundle\Repository\BeerSharingRepository")
 */
class BeerSharing
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="quantity", type="float", nullable=true)
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $quantity;
    
    /**
     * @ORM\OneToOne(targetEntity="Recipe", inversedBy="beerSharing")
     * @ORM\JoinColumn(name="recipe_id", referencedColumnName="id")
     */
    private $recipe;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param float $quantity
     *
     * @return BeerSharing
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set recipe
     *
     * @param \BrewnshareBundle\Entity\Recipe $recipe
     *
     * @return BeerSharing
     */
    public function setRecipe(\BrewnshareBundle\Entity\Recipe $recipe = null)
    {
        $this->recipe = $recipe;

        return $this;
    }

    /**
     * Get recipe
     *
     * @return \BrewnshareBundle\Entity\Recipe
     */
    public function getRecipe()
    {
        return $this->recipe;
    }
}
