<?php

namespace BrewnshareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UserHasDiscussion
 *
 * @ORM\Table(name="user_has_discussion")
 * @ORM\Entity(repositoryClass="BrewnshareBundle\Repository\UserHasDiscussionRepository")
 */
class UserHasDiscussion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="unread_messages", type="integer", nullable=true)
     */
    private $unreadMessages = 0;
    
    /**
     * @ORM\ManyToOne(targetEntity="Discussion", inversedBy="users")
     * @ORM\JoinColumn(name="discussion_id", referencedColumnName="id")
     */
    private $discussion;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="discussions")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set unreadMessages
     *
     * @param integer $unreadMessages
     *
     * @return UserHasDiscussion
     */
    public function setUnreadMessages($unreadMessages)
    {
        $this->unreadMessages = $unreadMessages;

        return $this;
    }

    /**
     * Get unreadMessages
     *
     * @return int
     */
    public function getUnreadMessages()
    {
        return $this->unreadMessages;
    }

    /**
     * Set discussion
     *
     * @param \BrewnshareBundle\Entity\Discussion $discussion
     *
     * @return UserHasDiscussion
     */
    public function setDiscussion(\BrewnshareBundle\Entity\Discussion $discussion = null)
    {
        $this->discussion = $discussion;

        return $this;
    }

    /**
     * Get discussion
     *
     * @return \BrewnshareBundle\Entity\Discussion
     */
    public function getDiscussion()
    {
        return $this->discussion;
    }

    /**
     * Set user
     *
     * @param \BrewnshareBundle\Entity\User $user
     *
     * @return UserHasDiscussion
     */
    public function setUser(\BrewnshareBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BrewnshareBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
