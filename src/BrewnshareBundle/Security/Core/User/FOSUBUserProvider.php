<?php

namespace BrewnshareBundle\Security\Core\User;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class FOSUBUserProvider extends BaseClass
{
    protected $em;
    protected $kernelRootDir;
    
    /**
     * Constructor.
     *
     * @param UserManagerInterface $userManager fOSUB user provider
     * @param array                $properties  property mapping
     */
    public function __construct(UserManagerInterface $userManager, array $properties, \Doctrine\ORM\EntityManager $em, $kernelRootDir)
    {
        parent::__construct($userManager, $properties);
        $this->em = $em;
        $this->kernelRootDir = $kernelRootDir;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $username = $response->getUsername();
        $resource = $response->getResourceOwner()->getName();
        
        if(null !== $username) {
            $user = $this->userManager->findUserBy(array($this->getProperty($response) => $username));
            // If not found by resource property, searching by email
            if (null === $user)
                $user = $this->userManager->findUserByEmail($response->getEmail());

            // If still not found, creating new User
            if (null === $user) {
                $user = $this->userManager->createUser();
                $user->setEmail($response->getEmail());
                $user->setPlainPassword(md5(uniqid($username, true)));
                $user->setEnabled(true);

                $this->updateInfos($user, $username, $response);

                $this->userManager->updateUser($user);
            }
            
            // If {resource}Id is not set, setting it and updating infos
            if($resource == 'facebook' && is_null($user->getFacebookId())) {
                $user->setFacebookId($username);
                $this->updateInfos($user, $username, $response);
                $this->userManager->updateUser($user);
            }
            
            if($resource == 'google' && is_null($user->getGoogleId())) {
                $user->setGoogleId($username);
                $this->updateInfos($user, $username, $response);
                $this->userManager->updateUser($user);
            }

            return $user;
        }
        else
            throw new AccountNotLinkedException(sprintf("User '%s' not found.", $username));
    }
    
    /**
     * Updating/Setting infos from facebook response
     */
    private function updateInfos(&$user, $username, $response) {
        if(is_null($user->getName()))
            $user->setName($response->getLastName());
        if(is_null($user->getSurname()))
            $user->setSurname($response->getFirstName());
        
        // Finding gender entity
//        if(is_null($user->getGender()) && isset($datas['gender'])) {
//            if($datas['gender'] == 'male')
//                $user->setGender('M');
//            if($datas['gender'] == 'female')
//                $user->setGender('F');
//        }
        
        // Getting profile picture
//        $photo = json_decode(file_get_contents("http://graph.facebook.com/".$username."/picture?height=311&type=normal&width=320&redirect=0"), true);
//        if(isset($photo['data']) && isset($photo['data']['url'])) {
//            $img_filename = uniqid().".jpg";
//            
//            file_put_contents($this->kernelRootDir."/../web/images/".$img_filename, file_get_contents($photo['data']['url']));
//            
//            $image->setName($img_filename);
//            $image->setOriginal($img_filename);
//            $this->em->persist($image);
//
//            $user->setImage($image);
//        }
    }
}