$(document).ready(function() {
    $('.btn-toggle').on('click', function() {
        var targetInput = $($(this).data('target'));
        
        targetInput.prop("checked", !targetInput.prop("checked"));
    });
});