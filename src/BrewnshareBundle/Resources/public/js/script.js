var initDatepickers = function() {
    $('.bns-datepicker').each(function(index) {
        $(this).datepicker({
            language: $('html')[0].lang,
            todayHighlight: true,
            autoclose: true,
            orientation: "bottom auto",
            defaultViewDate: {year: $(this).data('date-default-view-date-year'), month: $(this).data('date-default-view-date-month'), day: $(this).data('date-default-view-date-day')}
        });
        
        $(this).find('input').mask($(this).data('mask'));
    });
}

var deleteFormConfirm = function() {
    $('a.btn-delete-form').live('click', function() {
        var form = $(this).closest('form');
        
        swal({
            title: Translator.trans("Are you sure?"),
            text: form.data('confirm-text'),
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: 'btn-danger',
            confirmButtonText: Translator.trans('Delete!'),
            cancelButtonText: Translator.trans("Cancel")
        },
        function(isConfirm) {
            if(isConfirm) {
                // If ajax submit, sending delete form in ajax
                if(form.hasClass('ajax-submit')) {
                    $.ajax({
                        type: form.attr('method'),
                        url: form.attr('action'),
                        data: form.serialize(),
                        success: function(data) {
                            if(data == "reload")
                                location.reload();
                            else {
                                // If success, hiding element and displaying success message
                                $(form.data('target-element')).hide();
                                swal(Translator.trans("Deleted!"), data, "success");
                            }
                        },
                        error: function (data) {
                            swal(Translator.trans("Error!"), Translator.trans("An error occurred."), "danger");
                        }
                    });
                }
                else {
                    form.submit();
                }
            }
        });
    });
}

$(document).ready(function() {
    initDatepickers();
    deleteFormConfirm();
});