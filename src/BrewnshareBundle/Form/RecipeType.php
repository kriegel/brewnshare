<?php

namespace BrewnshareBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use BrewnshareBundle\Form\Type\DatePickerType;
use BrewnshareBundle\Form\Type\ToggleType;

class RecipeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('public', ToggleType::class, array(
                    'off' => "Private",
                    'on' => "Public",
                    'required' => false
                ))
            ->add('description')
            ->add('batchSize', NumberType::class, array(
                    'scale' => 2,
                    'required' => false
                ))
            ->add('abv', NumberType::class, array(
                    'scale' => 2,
                    'required' => false
                ))
            ->add('OG', NumberType::class, array(
                    'scale' => 3,
                    'required' => false
                ))
            ->add('OGUnit', ChoiceType::class, array(
                    'choices' => array('SG' => 'SG', 'Plato' => 'Plato'),
                    'required' => true
                ))
            ->add('FG', NumberType::class, array(
                    'scale' => 3,
                    'required' => false
                ))
            ->add('FGUnit', ChoiceType::class, array(
                    'choices' => array('SG' => 'SG', 'Plato' => 'Plato'),
                    'required' => true
                ))
            ->add('bitterness')
            ->add('bitternessUnit', ChoiceType::class, array(
                    'choices' => array('IBU' => 'IBU', 'EBU' => 'EBU'),
                    'required' => true
                ))
            ->add('color')
            ->add('colorUnit', ChoiceType::class, array(
                    'choices' => array('EBC' => 'EBC', 'SRM' => 'SRM'),
                    'required' => true
                ))
            ->add('brewDate', DatePickerType::class, array(
                    'required' => false
                ))
            ->add('bottledDate', DatePickerType::class, array(
                    'required' => false
                ))
//            ->add('awards')
//            ->add('brewerPartners', Select2EntityType::class, [
//                    'multiple' => true,
//                    'remote_route' => 'user_ajax_search',
//                    'class' => '\BrewnshareBundle\Entity\User',
//                    'primary_key' => 'id',
//                    'text_property' => 'name',
//                    'minimum_input_length' => 2,
//                    'page_limit' => 10,
//                    'allow_clear' => true,
//                    'delay' => 250,
//                    'cache' => true,
//                    'cache_timeout' => 60000, // if 'cache' is true
//                    'language' => 'fr',
//                    'placeholder' => 'Ajouter un utilisateur collaborateur'
//                ])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BrewnshareBundle\Entity\Recipe'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'brewnsharebundle_recipe';
    }


}
