<?php

namespace BrewnshareBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use BrewnshareBundle\Form\AddressType;
use BrewnshareBundle\Form\EquipmentType;
use BrewnshareBundle\Form\Type\DatePickerType;


class ProfileType extends AbstractType {
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->remove('username')
                ->remove('current_password')
                ->add('gender', ChoiceType::class, array(
                    'choices' => array('Female' => 'F', 'Male' => 'M'),
                    'placeholder' => false,
                    'expanded' => true,
                    'required' => false
                ))
                ->add('surname')
                ->add('name')
                ->add('birthdate', DatePickerType::class, array(
                    'startView' => 'years',
                    'defaultViewDate' => array('year' => (date("Y") - 30), 'month' => 0, 'day' => 1),
                    'endDate' => date("d/m/Y"),
                    'required' => false
                ))
                ->add('avatar', FileType::class, array(
                    'required' => false
                ))
                ->add('mainAddress', AddressType::class, array(
                    'required' => false
                ))
                ->add('biography', TextareaType::class, array(
                    'required' => false
                ))
                ->add('alertRange', IntegerType::class, array(
                    'required' => false
                ))
                ->add('equipment', EquipmentType::class)
                ->add('plainPassword', RepeatedType::class, array(
                    'type' => PasswordType::class,
                    'options' => array('translation_domain' => 'FOSUserBundle'),
                    'first_options' => array('label' => 'form.password'),
                    'second_options' => array('label' => 'form.password_confirmation'),
                    'invalid_message' => 'fos_user.password.mismatch',
                    'required' => false
                ))
            ;
    }
    
    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
    }
    
    public function getBlockPrefix()
    {
        return 'bns_user_profile';
    }
}
