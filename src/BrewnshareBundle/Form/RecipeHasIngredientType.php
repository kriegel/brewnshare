<?php

namespace BrewnshareBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class RecipeHasIngredientType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ingredient', EntityType::class, array(
                'class' => 'Brewnshare\Entity\Ingredient',
                'property' => 'name',
//                'query_builder' => function (EntityRepository $er) {
//                
//                },
            ))
            ->add('name', HiddenType::class, array(
                'required' => false
            ))
            ->add('quantity')
//            ->add('weightUnit', ChoiceType::class, array(
//                    'choices' => array('EBC' => 'EBC', 'SRM' => 'SRM'),
//                    'required' => true
//                ))
            ->add('time')
            ->add('type');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BrewnshareBundle\Entity\Ingredient'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'brewnsharebundle_ingredient';
    }


}
