<?php

namespace BrewnshareBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ToggleType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'off' => "Off",
            'on' => "On",
            'required' => false
        ));
    }
    
    /**
     * Pass the format to the view
     *
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['on'] = isset($options['on']) ? $options['on'] : "On";
        $view->vars['off'] = isset($options['off']) ? $options['off'] : "Off";
    }
    
    public function getBlockPrefix() {
        return 'toggle';
    } 
    
    public function getParent() {
        return CheckboxType::class;
    }
}