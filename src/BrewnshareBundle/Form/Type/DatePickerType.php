<?php

namespace BrewnshareBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class DatePickerType extends AbstractType
{
    protected $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $request = $this->requestStack->getCurrentRequest();
        
        switch($request->getLocale()) {
//            case 'en_US':
//                $format = 'MM/dd/yyyy';
//                break;
            
            default:
                $format = 'dd/mm/yyyy';
                break;
        }
        
        // Generating mask for masked-input plugin
        $mask = preg_replace("#(dd|mm|yy)#", '99', $format);
        
        $resolver->setDefaults(array(
            'widget' => "single_text",
            'format' => $format,
            'startView' => 'days',
            'defaultViewDate' => array('year' => date("Y"), 'month' => (date("m") - 1), 'day' => date('d')),
            'endDate' => '',
            'mask' => $mask,
            'placeholder' => $format,
            'html5' => false,
            'required' => false
        ));
    }
    
    /**
     * Pass the format to the view
     *
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['format'] = isset($options['format']) ? $options['format'] : "";
        $view->vars['startView'] = isset($options['startView']) ? $options['startView'] : 'days';
        $view->vars['defaultViewDate'] = isset($options['defaultViewDate']) ? $options['defaultViewDate'] : array('year' => date("Y"), 'month' => (date("m") - 1), 'day' => date('d'));
        $view->vars['endDate'] = isset($options['endDate']) ? $options['endDate'] : '';
        $view->vars['mask'] = isset($options['mask']) ? $options['mask'] : "";
    }
    
    public function getBlockPrefix() {
        return 'datepicker';
    } 
    
    public function getParent() {
        return DateType::class;
    }
}