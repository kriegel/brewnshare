<?php

namespace BrewnshareBundle\Repository;

/**
 * IngredientRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class IngredientRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Find a Ingredient entity by its name for a type and locale
     * 
     * @param integer $type
     * @param string $locale
     * @param string $name
     * 
     * @return Ingredient|null
     */
    public function findOneByLocaleName($type, $locale, $name, $laboratory = null, $productId = null) {
        $qb = $this->createQueryBuilder('i')
                    ->select('i')
                    ->leftJoin('i.translations', 't')
                    ->where('i.mainType = :type')
                    ->andWhere('t.locale = :locale')
                    ->andWhere('t.name LIKE :name')
                    ->setMaxResults(1)
                    ->setParameters(array(
                        'type' => $type,
                        'locale' => $locale,
                        'name' => trim($name)
                    ));
                    
        if(!is_null($laboratory))
            $qb->andWhere('i.laboratory LIKE :laboratory')
                ->setParameter('laboratory', trim($laboratory));
                    
        if(!is_null($productId))
            $qb->andWhere('i.productId LIKE :productId')
                ->setParameter('productId', trim($productId));
        
        return $qb->getQuery()->getOneOrNullResult();
    }
    
    /**
     * Find a Ingredient entity by its name for a type, without locale filtering
     * 
     * @param integer $type
     * @param string $locale
     * @param string $name
     * 
     * @return Ingredient|null
     */
    public function findOneByName($type, $name, $laboratory = null, $productId = null) {
        $qb = $this->createQueryBuilder('i')
                    ->select('i')
                    ->leftJoin('i.translations', 't')
                    ->where('i.mainType = :type')
                    ->andWhere('t.name LIKE :name')
                    ->setMaxResults(1)
                    ->setParameters(array(
                        'type' => $type,
                        'name' => trim($name)
                    ));
                    
        if(!is_null($laboratory))
            $qb->andWhere('i.laboratory LIKE :laboratory')
                ->setParameter('laboratory', trim($laboratory));
                    
        if(!is_null($productId))
            $qb->andWhere('i.productId LIKE :productId')
                ->setParameter('productId', trim($productId));

        return $qb->getQuery()->getOneOrNullResult();
    }
}
