<?php

namespace BrewnshareBundle\Tools;

class IngredientType {
 
   const CONSTANT_TYPE_FERMENTABLE = 10;
   const CONSTANT_TYPE_HOP = 20;
   const CONSTANT_TYPE_YEAST = 30;
   const CONSTANT_TYPE_MISC = 40;
   
   public static function getTypeArray() {

      $mType = array(
            self::CONSTANT_TYPE_FERMENTABLE => "Fermentable",
            self::CONSTANT_TYPE_HOP => "Hop",
            self::CONSTANT_TYPE_YEAST => "Yeast",
            self::CONSTANT_TYPE_MISC => "Misc",
      );

      return $mType;
   }
   
   public static function getType($type) {
      
      $a = self::getTypeArray();
      
      if (!array_key_exists($type, $a))
         return "AUCUN TYPE";
      else
         return $a[$type];
   }
   
   public static function getTypeValue($type) {
       return constant("self::".$type);
   }
}

?>