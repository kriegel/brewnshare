<?php

namespace BrewnshareBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use BrewnshareBundle\Entity\User;
use BrewnshareBundle\Model\UserControllerInterface;

class BaseUserController extends Controller implements UserControllerInterface
{
    protected $routeUser;
    
    public function setRouteUser(User $user) {
        $this->routeUser = $user;
    }
    
    public function setDefaultUser() {
        $this->setRouteUser($this->getUser());
    }
    
    public function getRouteUser() {
        return $this->routeUser;
    }
    
    public function isCurrentUser() {
        return $this->getRouteUser() == $this->getUser();
    }
}