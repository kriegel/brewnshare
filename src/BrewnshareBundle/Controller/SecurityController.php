<?php

namespace BrewnshareBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;

class SecurityController extends BaseController
{
    /**
     * If user is already connected, redirecting to homepage rather than displaying login page
     */
    protected function renderLogin(array $data)
    {
        if($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
            return $this->redirectToRoute('brewnshare_homepage');
        
        return parent::renderLogin($data);
    }
}
