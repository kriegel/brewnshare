<?php

namespace BrewnshareBundle\Controller;

use BrewnshareBundle\Controller\BaseUserController;

class DefaultController extends BaseUserController
{
    public function homepageAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
            return $this->forward('BrewnshareBundle:Default:homepageAnonymous');
        
        return $this->render('BrewnshareBundle:Default:homepage_user.html.twig');
    }
    
    public function homepageAnonymousAction()
    {
        return $this->render('BrewnshareBundle:Default:homepage_anonymous.html.twig');
    }
}
