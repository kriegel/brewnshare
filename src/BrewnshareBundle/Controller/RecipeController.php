<?php

namespace BrewnshareBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use BrewnshareBundle\Controller\BaseUserController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

use BrewnshareBundle\Entity\Recipe;
use BrewnshareBundle\Tools\IngredientType;

/**
 * Recipe controller.
 */
class RecipeController extends BaseUserController
{
    /**
     * Lists all recipe entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $paginator = $this->get('knp_paginator');
        $query = $em->getRepository('BrewnshareBundle:Recipe')->searchByUserQuery($this->getRouteUser());
        $recipes = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );
        
        $deleteForms = array();
        foreach($recipes as $recipe)
            $deleteForms[$recipe->getId()] = $this->createDeleteForm($recipe)->createView();

        return $this->render('BrewnshareBundle:recipe:index.html.twig', array(
            'recipes' => $recipes,
            'deleteForms' => $deleteForms
        ));
    }

    /**
     * Creates a new recipe entity.
     *
     */
    public function newAction(Request $request)
    {
        $recipe = new Recipe();
        $recipe->setUser($this->getUser());
        $form = $this->createForm('BrewnshareBundle\Form\RecipeType', $recipe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($recipe);
            $em->flush($recipe);

            return $this->redirectToRoute('recipe_show', array('slug_user' => $this->getRouteUser()->getSlug(), 'slug' => $recipe->getSlug()));
        }
        
        $importForm = $this->createImportForm();

        return $this->render('BrewnshareBundle:recipe:new.html.twig', array(
            'recipe' => $recipe,
            'form' => $form->createView(),
            'import_form' => $importForm->createView(),
        ));
    }

    /**
     * Creates a new recipe entity.
     *
     */
    public function importAction(Request $request)
    {
        $recipe = new Recipe();
        $recipe->setUser($this->getUser());
        $importForm = $this->createImportForm();
        $importForm->handleRequest($request);

        if ($importForm->isSubmitted() && $importForm->isValid()) {
            $this->get('brewnshare.recipe_manager')->importFromXmlFile($recipe, $importForm->get('file')->getData());   

            return $this->redirectToRoute('recipe_show', array('slug_user' => $this->getRouteUser()->getSlug(), 'slug' => $recipe->getSlug()));
        }
        
        $form = $this->createForm('BrewnshareBundle\Form\RecipeType', $recipe);

        return $this->render('BrewnshareBundle:recipe:new.html.twig', array(
            'recipe' => $recipe,
            'form' => $form->createView(),
            'import_form' => $importForm->createView(),
        ));
    }

    /**
     * Finds and displays a recipe entity.
     */
    public function showAction(Request $request, $slug)
    {
        $em = $this->getDoctrine()->getManager();
        
        if($this->isCurrentUser())
            $recipe = $em->getRepository('BrewnshareBundle:Recipe')->findOneBy(array('user' => $this->getUser(), 'slug' => $slug));
        else
            $recipe = $em->getRepository('BrewnshareBundle:Recipe')->findOneBy(array('user' => $this->getRouteUser(), 'slug' => $slug, 'public' => true));
        
        if(!$recipe)
            throw $this->createNotFoundException('No recipe found for slug '.$slug);
        
        $fermantables = $em->getRepository('BrewnshareBundle:RecipeHasIngredient')->findBy(array('recipe' => $recipe, 'mainType' => IngredientType::CONSTANT_TYPE_FERMENTABLE), array('addAfterBoil' => "ASC", 'quantity' => "DESC"));
        $yeasts = $em->getRepository('BrewnshareBundle:RecipeHasIngredient')->findBy(array('recipe' => $recipe, 'mainType' => IngredientType::CONSTANT_TYPE_YEAST), array('quantity' => "DESC"));

        /**
         * Hops management
         */
        $hops = array('Mash' => array(), 'First Wort' => array(), 'Boil' => array(), 'Aroma' => array(), 'Dry Hop' => array(), 'Other' => array());
        $hopsRaw = $em->getRepository('BrewnshareBundle:RecipeHasIngredient')->findBy(array('recipe' => $recipe, 'mainType' => IngredientType::CONSTANT_TYPE_HOP), array('time' => "DESC"));
        // Sorting by use for display
        foreach($hopsRaw as $hop) {
            if(!array_key_exists($hop->getUse(), $hops))
                $hops['Other'][] = $hop;
            else
                $hops[$hop->getUse()][] = $hop;
        }

        /**
         * Miscs management
         */
        $miscs = array('Mash' => array(), 'Boil' => array(), 'Primary' => array(), 'Secondary' => array(), 'Bottling' => array(), 'Other' => array());
        $miscsRaw = $em->getRepository('BrewnshareBundle:RecipeHasIngredient')->findBy(array('recipe' => $recipe, 'mainType' => IngredientType::CONSTANT_TYPE_MISC), array('time' => "DESC"));
        // Sorting by use for display
        foreach($miscsRaw as $misc) {
            if(!array_key_exists($misc->getUse(), $miscs))
                $miscs['Other'][] = $misc;
            else
                $miscs[$misc->getUse()][] = $misc;
        }

        $deleteForm = $this->isCurrentUser() ? $this->createDeleteForm($recipe) : null;
        
        return $this->render('BrewnshareBundle:recipe:show.html.twig', array(
            'recipe' => $recipe,
            'fermentables' => $fermantables,
            'hops' => $hops,
            'miscs' => $miscs,
            'yeasts' => $yeasts,
            'delete_form' => is_null($deleteForm) ? null : $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing recipe entity.
     *
     */
    public function editAction(Request $request, $slug)
    {
        $em = $this->getDoctrine()->getManager();
        
        $recipe = $em->getRepository('BrewnshareBundle:Recipe')->findOneBy(array('user' => $this->getUser(), 'slug' => $slug));
        if(!$recipe)
            throw $this->createNotFoundException('No recipe found for slug '.$slug);
        
        $deleteForm = $this->createDeleteForm($recipe);
        $editForm = $this->createForm('BrewnshareBundle\Form\RecipeType', $recipe);
        $editForm->add('save', SubmitType::class, array(
                'label' => 'Update',
                'attr' => array('class' => "btn btn-primary pull-right")
            ));
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            
            $this->addFlash('success', 'Your changes were saved!');

            return $this->redirectToRoute('recipe_edit', array('slug_user' => $this->getUser()->getSlug(), 'slug' => $recipe->getSlug()));
        }

        return $this->render('BrewnshareBundle:recipe:edit.html.twig', array(
            'recipe' => $recipe,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a recipe entity.
     *
     */
    public function deleteAction(Request $request, Recipe $recipe)
    {
        $form = $this->createDeleteForm($recipe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($recipe);
            $em->flush();
            
            $deleteMessage = $this->get('translator')->trans("recipes.delete.success_text");
            if($request->isXmlHttpRequest())
                return new JsonResponse($deleteMessage);
            else
                $this->addFlash('success', $deleteMessage);
        }

        return $this->redirectToRoute('recipe_index', array('slug_user' => $this->getRouteUser()->getSlug()));
    }

    /**
     * Creates a form to delete a recipe entity.
     *
     * @param Recipe $recipe The recipe entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Recipe $recipe)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('recipe_delete', array('slug_user' => $this->getUser()->getSlug(), 'slug' => $recipe->getSlug())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Creates a form to add a recipe entity by file import.
     *
     * @param Recipe $recipe The recipe entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createImportForm()
    {
        return $this->createFormBuilder()
                ->setAction($this->generateUrl('recipe_import'))
                ->setMethod('POST')
                ->add('file', FileType::class, array(
                    'required' => true
                ))
                ->getForm()
            ;
    }
}
