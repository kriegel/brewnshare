<?php

namespace BrewnshareBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use BrewnshareBundle\Util\XmlTools;

/**
 * Command to import ingredients from xml file
 *
 * @author kevinriegel
 */
class importIngredientsCommand extends ContainerAwareCommand {
    
    protected function configure() {
        $this
            ->setName('bns:import:ingredients')
            ->setDescription('Import ingredients from xml file')
            ->addArgument(
                'file', InputArgument::REQUIRED, 'Filename you want to import (stored in import/ folder)'
            )
            ->addArgument(
                'locale', InputArgument::OPTIONAL, 'In which locale/language you want to import (default: fr)'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $io = new SymfonyStyle($input, $output);
        $io->section('Start of import');
        
        $fs = new Filesystem();
        $file = $this->getContainer()->get('kernel')->getRootDir().'/../imports/'.$input->getArgument('file');
        
        if($fs->exists($file)) {
            $xml = XmlTools::loadXml($file);
            $locale = $input->getArgument('locale');
            if(!$locale)
                $locale = 'fr';
            
            /**
             * Fermentables import
             */
            $io->section('Fermentables import');
            $io->progressStart(sizeof($xml->FERMENTABLE));
            foreach($xml->FERMENTABLE as $fermentable) {
                
                if(empty($fermentable->NAME))
                    continue;
                
                $oFermentable = $this->getContainer()->get('brewnshare.ingredient_manager')->getFermentableObjectFromXMl($fermentable, $locale, true);
                
                $io->progressAdvance();
            }
            $io->progressFinish();
            
            /**
             * Hops import
             */
            $io->section('Hops import');
            $io->progressStart(sizeof($xml->HOP));
            foreach($xml->HOP as $hop) {
                
                if(empty($hop->NAME))
                    continue;
                
                $oHop = $this->getContainer()->get('brewnshare.ingredient_manager')->getHopObjectFromXMl($hop, $locale, true);
                
                $io->progressAdvance();
            }
            $io->progressFinish();
            
            /**
             * Yeasts import
             */
            $io->section('Yeasts import');
            $io->progressStart(sizeof($xml->YEAST));
            foreach($xml->YEAST as $yeast) {
                
                if(empty($yeast->NAME))
                    continue;
                
                $oYeast = $this->getContainer()->get('brewnshare.ingredient_manager')->getYeastObjectFromXMl($yeast, $locale, true);
                
                $io->progressAdvance();
            }
            $io->progressFinish();
            
            /**
             * Miscs import
             */
            $io->section('Miscs import');
            $io->progressStart(sizeof($xml->MISC));
            foreach($xml->MISC as $misc) {
                
                if(empty($misc->NAME))
                    continue;
                
                $oMisc = $this->getContainer()->get('brewnshare.ingredient_manager')->getMiscObjectFromXMl($misc, $locale, true);
                
                $io->progressAdvance();
            }
            $io->progressFinish();
            
            $io->success('Success of import');
        }
        else
            $io->error('File does not exist');
        
        $io->section('End of import');
    }
}
