<?php

namespace BrewnshareBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use BrewnshareBundle\Util\XmlTools;

/**
 * Command to import ingredients from xml file
 *
 * @author kevinriegel
 */
class importStylesCommand extends ContainerAwareCommand {
    
    protected function configure() {
        $this
            ->setName('bns:import:styles')
            ->setDescription('Import styles from xml file')
            ->addArgument(
                'file', InputArgument::REQUIRED, 'Filename you want to import (stored in import/ folder)'
            )
            ->addArgument(
                'locale', InputArgument::OPTIONAL, 'In which locale/language you want to import (default: fr)'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $io = new SymfonyStyle($input, $output);
        $io->section('Start of import');
        
        $fs = new Filesystem();
        $file = $this->getContainer()->get('kernel')->getRootDir().'/../imports/'.$input->getArgument('file');
        
        if($fs->exists($file)) {
            $xml = XmlTools::loadXml($file);
            $locale = $input->getArgument('locale');
            if(!$locale)
                $locale = 'fr';
            
            /**
             * Fermentables import
             */
            $io->section('Styles import');
            $io->progressStart(sizeof($xml->STYLE));
            foreach($xml->STYLE as $style) {
                
                if(empty($style->NAME))
                    continue;
                
                $oStyle = $this->getContainer()->get('brewnshare.style_manager')->getStyleObjectFromXMl($style, $locale, true);
                
                $io->progressAdvance();
            }
            $io->progressFinish();
        }
        else
            $io->error('File does not exist');
        
        $io->section('End of import');
    }
}
