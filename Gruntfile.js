module.exports = function(grunt) { 
  // Chargement automatique de tous nos modules
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    uglify: {
        options: {
          mangle: false
        },
        myjs: {  
            options: {
              beautify: false
            },
          files: {
            'web/built/js/script.min.js': [
              'src/BrewnshareBundle/Resources/public/js/script.js',
              'src/BrewnshareBundle/Resources/public/js/bootstrap-toggle.js'
            ],
            'web/built/js/password-indicator-custom.min.js': [
              'src/BrewnshareBundle/Resources/public/js/password-indicator-custom.js'
            ],
          }
        }
      },    
    cssmin: {
      combine: {
        options:{
          report: 'gzip',
          keepSpecialComments: 0
        },
        files: [{
            'web/built/css/style.css': [
              'src/BrewnshareBundle/Resources/public/css/style.css',
            ],
            'web/built/css/bootstrap-toggle.css': [
              'src/BrewnshareBundle/Resources/public/css/bootstrap-toggle.css',
            ]
        },

        ]
      }
    }, //end cssmin
    watch: {
      css: {
        files: ['src/BrewnshareBundle/Resources/**/*.scss', 'src/BrewnshareBundle/Resources/**/*.css'],
        tasks: ['css']
      },
      javascript: {
        files: ['src/BrewnshareBundle/Resources/public/js/*.js'],
        tasks: ['javascript']
      }
    },
    copy: {
      dist: {
        files: [{
            expand: true,
            cwd: 'src/BrewnshareBundle/Resources/public/plugins/',
            src: ['**'],
            dest: 'web/assets/plugins/'
        },
        {
            expand: true,
            cwd: 'src/BrewnshareBundle/Resources/public/bootstrap/', 
            src: ['**'],
            dest: 'web/assets/bootstrap/'
        }
        ]
      }     
    }
  });

  // Déclaration des différentes tâches
  grunt.registerTask('javascript', ['uglify']);
  grunt.registerTask('css', ['cssmin']);
  grunt.registerTask('cp', ['copy']);
  grunt.registerTask('default', ['css','javascript','cp']);

};
